package net.beastcube.minigameapi.countdowns;

import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author BeastCube
 */
public class ArenaCountdown extends Countdown {
    public ArenaCountdown(int time) {
        super(time);
    }

    @Override
    public void callback() {

    }

    @Override
    public void showTime(int time, String timeAsString) {
        Util.broadcastActionBarMessage(ChatColor.GOLD + "Das Spiel startet in " + timeAsString);
        if (time <= 10)
            Bukkit.broadcastMessage(ChatColor.GOLD + "Das Spiel startet in " + timeAsString);
    }

    @Override
    public boolean start() {
        return MinigameAPI.getInstance().setState(MinigameState.ARENA_COUNTDOWN);
    }
}
