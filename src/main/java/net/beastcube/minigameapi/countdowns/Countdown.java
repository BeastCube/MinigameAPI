package net.beastcube.minigameapi.countdowns;

import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

/**
 * A simple class that handles countdowns. Use it like that: <br\>
 * <p>
 * Countdown countdown = new Countdown(60, new Runnable() {
 * >@Override
 * <p/>
 * <p/>
 * }
 * </p>
 */
public abstract class Countdown {

    final int ticktime = 20;

    private int time;
    private int maxTime;
    private int task = -1;
    private boolean running = false;

    public Countdown(int time) {
        this.time = time;
        maxTime = time;
    }

    final Runnable run = new Runnable() {

        public void run() {
            showTime(time, ((time % 3600 == 0 && time > 0) ? (time / 3600 + " Stunden") : (time % 60 == 0) && (time > 0) ? (time / 60 + " Minuten") : (time + " Sekunden")));
            time--;
            if (time <= 0) {
                cancel();
                callback();
                Countdowns.next();
            }
        }

    };

    public void start(Plugin plugin) {
        if (maxTime > -1 && start()) {
            task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, run, 0, ticktime);
            running = true;
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void cancel() {
        if (task != -1) {
            Bukkit.getScheduler().cancelTask(task);
            running = false;
            time = maxTime;
        }
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


    public void showTime(int time, String timeAsString) {
        Util.broadcastActionBarMessage(ChatColor.GOLD + "" + timeAsString);
        if (time <= 10)
            Bukkit.broadcastMessage(ChatColor.GOLD + "" + time + " Sekunden");
    }

    public abstract void callback();

    public abstract boolean start();

}
