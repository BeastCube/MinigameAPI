package net.beastcube.minigameapi.countdowns;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.kits.KitChoosing;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.team.TeamChoosing;
import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 * @author BeastCube
 */
public class LobbyCountdown2 extends Countdown {

    public LobbyCountdown2(int time) {
        super(time);
    }

    @Override
    public void showTime(int time, String timeAsString) {
        if (time <= 5 || time == 10) {
            String message = ChatColor.GOLD + "Das Spiel startet in " + ChatColor.GREEN + time + " Sekunden";
            Util.broadcastActionBarMessage(message);
            Bukkit.broadcastMessage(message);
        }
    }

    @Override
    public void callback() {
        TeamChoosing.setTeams();
        KitChoosing.applyKits();
        MinigameAPI.getInstance().getOnlinePlayers().forEach(p -> {
            p.setGameMode(MinigameAPI.getInstance().getDefaults().getDefaultGamemode());
            BeastCubeAPI.disableLobbyFeatures(p);
            Location l = MinigameAPI.getInstance().getArena().getSpawn(p);
            p.teleport(l);
        });
    }

    @Override
    public boolean start() {
        if (!MinigameAPI.getInstance().setState(MinigameState.LOBBY_COUNTDOWN_2))
            return false;
        MapChoosing.close();
        MinigameAPI.getInstance().loadRandomArena();
        Util.broadcastTitleMessage(new TitleObject(ChatColor.DARK_PURPLE + "Map: " + ChatColor.GOLD + MinigameAPI.getInstance().getArena().getName(), ""));
        MinigameAPI.getInstance().getOnlinePlayers().forEach(MapChoosing::takeFromPlayer);
        return true;
    }
}
