package net.beastcube.minigameapi.countdowns;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.util.IO;
import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.io.File;

/**
 * @author BeastCube
 */
public class RestartCountdown extends Countdown {
    public RestartCountdown(int time) {
        super(time);
    }

    @Override
    public void callback() {
        MinigameAPI.getInstance().getOnlinePlayers().forEach(BeastCubeAPI::connectToHub);
        //Delaying the shutdown so that the players will be connected to the hub server even with lags.
        Bukkit.getScheduler().scheduleSyncDelayedTask(MinigameAPI.getInstance().getMinigame(), () -> {
            File worldFile = new File(MinigameAPI.getInstance().getArena().getName());
            if (worldFile.exists())
                IO.deleteFolder(worldFile);
            Bukkit.getServer().shutdown();
        }, 200);
    }

    @Override
    public void showTime(int time, String timeAsString) {
        if (time == 15 || time == 10 || time <= 5)
            Util.broadcastActionBarMessage(ChatColor.RED + "Server restartet in " + time + " Sekunden.");
    }

    @Override
    public boolean start() {
        return MinigameAPI.getInstance().setState(MinigameState.RESTARTING);
    }
}
