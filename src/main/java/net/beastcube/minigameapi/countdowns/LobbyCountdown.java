package net.beastcube.minigameapi.countdowns;

import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author BeastCube
 */
public class LobbyCountdown extends Countdown {

    public LobbyCountdown(int time) {
        super(time);
    }

    @Override
    public void showTime(int time, String timeAsString) {
        if (time > 10 && time % 10 == 0) {
            String message = ChatColor.GOLD + "Das Spiel startet in " + ChatColor.GREEN + time + " Sekunden";
            Util.broadcastActionBarMessage(message);
            Bukkit.broadcastMessage(message);
        }

    }

    @Override
    public void callback() {

    }

    @Override
    public boolean start() {
        return MinigameAPI.getInstance().setState(MinigameState.LOBBY_COUNTDOWN_1);
    }
}
