package net.beastcube.minigameapi.countdowns;

import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.events.GameStartEvent;
import net.beastcube.minigameapi.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @author nDepedend
 */
public class IngameCountdown extends Countdown {

    public IngameCountdown(int time) {
        super(time);
    }

    @Override
    public void callback() {
    }

    @Override
    public void showTime(int time, String timeAsString) {
        if (time <= 10) {
            Util.broadcastActionBarMessage(ChatColor.GOLD + "Das Spiel endet in " + timeAsString);
            Bukkit.broadcastMessage(ChatColor.GOLD + "Das Spiel endet in " + timeAsString);
        } else if (time <= 60 && time % 10 == 0 || time <= 600 && time % 60 == 0 || time % 600 == 0) {
            Bukkit.broadcastMessage(ChatColor.GOLD + "Das Spiel endet in " + timeAsString);
        }
    }

    @Override
    public boolean start() {
        for (Player p : MinigameAPI.getInstance().getPlayersWithState(PlayerState.IN_LOBBY))
            MinigameAPI.getInstance().setPlayerState(p, PlayerState.INGAME);
        GameStartEvent event = new GameStartEvent(new TitleObject(ChatColor.GREEN + "Das Spiel hat begonnen!", ""));
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled() && MinigameAPI.getInstance().setState(MinigameState.INGAME)) {
            Util.broadcastTitleMessage(event.getMessage());
            return true;
        }
        return false;
    }
}
