package net.beastcube.minigameapi.countdowns;

import lombok.Getter;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 */
public final class Countdowns {

    public static Countdown lobbyCountdown1;
    public static Countdown lobbyCountdown2;
    public static Countdown arenaCountdown;
    public static Countdown ingameCountdown;
    public static Countdown restartCountdown;

    private static Countdown countdown;

    public static void startTimeline() {
        current = 0;
        countdown = getCountdowns().get(current);
        if (countdown != null)
            countdown.start(MinigameAPI.getInstance().getMinigame());
    }

    public static void stopCountdown() {
        if (countdown != null && countdown.isRunning())
            countdown.cancel();
    }

    public static Countdown getCountdown() {
        return countdown;
    }

    public static void setCountdown(Countdown countdown) {
        Countdowns.countdown = countdown;
    }

    @Getter
    private static final List<Countdown> countdowns = new ArrayList<Countdown>();

    public static void addCountdown(int index, Countdown countdown) {
        countdowns.add(index, countdown);
    }

    public static void addCountdown(Countdown after, Countdown countdown) {
        countdowns.add(countdowns.indexOf(after) + 1, countdown);
    }

    public static void replaceCountdown(Countdown toreplace, Countdown with) {
        countdowns.set(countdowns.indexOf(toreplace), with);
    }

    public static void removeCountdown(Countdown toremove) {
        countdowns.remove(toremove);
    }

    public static void clearCountdowns() {
        countdowns.clear();
    }

    private static int current = -1;

    public static void next() {
        current++;
        if (getCountdowns().size() > current) {
            countdown = getCountdowns().get(current);
            countdown.start(MinigameAPI.getInstance().getMinigame());
        } else {
            if (MinigameAPI.getInstance().getState() == MinigameState.INGAME)
                MinigameAPI.getInstance().endGame(null);
        }
    }

    static {
        lobbyCountdown1 = new LobbyCountdown(40);

        lobbyCountdown2 = new LobbyCountdown2(10);

        arenaCountdown = new ArenaCountdown(10);

        ingameCountdown = new IngameCountdown(MinigameAPI.getInstance().getDefaults().getIngameCountdown());

        restartCountdown = new RestartCountdown(16);

        getCountdowns().add(lobbyCountdown1);
        getCountdowns().add(lobbyCountdown2);
        getCountdowns().add(arenaCountdown);
        getCountdowns().add(ingameCountdown);
    }

}
