package net.beastcube.minigameapi.entityregistry;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.Entity;

/**
 * @author BeastCube
 */
public class EntityRegistrationEntry {

    @Getter
    private String name;
    @Getter
    private int registrationId;
    @Getter
    private Entity entity;

    public EntityRegistrationEntry(String name, int registrationId, Entity entity) {
        if (entity == null) {
            throw new EntityRegistrationException("Pet type is not supported by this server version.");
        }

        this.name = name;
        this.registrationId = registrationId;
        this.entity = entity;
    }
}