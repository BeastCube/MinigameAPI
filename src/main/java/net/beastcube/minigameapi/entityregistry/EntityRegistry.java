package net.beastcube.minigameapi.entityregistry;

import net.beastcube.api.bukkit.util.EntityMapModifier;
import net.beastcube.api.bukkit.util.ReflectionUtils;
import net.minecraft.server.v1_8_R3.Entity;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @author BeastCube
 *         <p>
 *         Reversible registration of entities to Minecraft internals. Allows for temporary modification of internal mappings
 *         so
 *         that custom pet entities can be spawned.
 *         <p>
 *         NOTE: This class is a modified version of the registry used in EchoPet v3.
 */
@SuppressWarnings("unchecked")
public class EntityRegistry {

    private static final EntityMapModifier<Class<?>, String> CLASS_TO_NAME_MODIFIER;
    private static final EntityMapModifier<Class<?>, Integer> CLASS_TO_ID_MODIFIER;
    private static final EntityMapModifier<Integer, Class<?>> ID_TO_CLASS_MODIFIER;

    static {
        try {
            Class<?> entityTypes = ReflectionUtils.PackageType.MINECRAFT_SERVER.getClass("EntityTypes");
            List<Field> typeMaps = new ArrayList<>();
            for (Field candidate : entityTypes.getDeclaredFields()) {
                if (Map.class.isAssignableFrom(candidate.getType())) {
                    candidate.setAccessible(true);
                    typeMaps.add(candidate);
                }
            }
            CLASS_TO_NAME_MODIFIER = new EntityMapModifier<>((Map<Class<?>, String>) typeMaps.get(1).get(null));
            ID_TO_CLASS_MODIFIER = new EntityMapModifier<>((Map<Integer, Class<?>>) typeMaps.get(2).get(null));
            CLASS_TO_ID_MODIFIER = new EntityMapModifier<>((Map<Class<?>, Integer>) typeMaps.get(3).get(null));
        } catch (IllegalAccessException | ClassNotFoundException e) {
            throw new IllegalStateException("Failed to initialise Entity type maps correctly!", e);
        }
    }

    public static Entity spawn(EntityRegistrationEntry registrationEntry, final Location location) {
        if (registrationEntry == null) {
            // Pet type not registered
            return null;
        }

        return performRegistration(registrationEntry, () -> {
            ((CraftWorld) location.getWorld()).getHandle().addEntity(registrationEntry.getEntity(), CreatureSpawnEvent.SpawnReason.CUSTOM);
            registrationEntry.getEntity().setPositionRotation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
            return registrationEntry.getEntity();
        });
    }

    public static <T> T performRegistration(EntityRegistrationEntry registrationEntry, Callable<T> callable) {

        CLASS_TO_NAME_MODIFIER.modify(registrationEntry.getEntity().getClass(), registrationEntry.getName());
        CLASS_TO_ID_MODIFIER.modify(registrationEntry.getEntity().getClass(), registrationEntry.getRegistrationId());

        Class<?> existingEntityClass = ID_TO_CLASS_MODIFIER.getMap().get(registrationEntry.getRegistrationId());
        // Just to be sure, remove any existing mappings and replace them afterwards
        // Make this entity the 'default' while the entity is being spawned
        ID_TO_CLASS_MODIFIER.clear(existingEntityClass);
        ID_TO_CLASS_MODIFIER.modify(registrationEntry.getRegistrationId(), registrationEntry.getEntity().getClass());

        try {
            ID_TO_CLASS_MODIFIER.applyModifications();

            CLASS_TO_NAME_MODIFIER.applyModifications();
            CLASS_TO_ID_MODIFIER.applyModifications();

            return callable.call();
        } catch (Exception e) {
            throw new EntityRegistrationException(e);
        } finally {
            // Ensure everything is back to normal
            // Client will now receive the correct entityregistry ID and we're all set!
            ID_TO_CLASS_MODIFIER.removeModifications();
            ID_TO_CLASS_MODIFIER.add(existingEntityClass);

            CLASS_TO_NAME_MODIFIER.removeModifications();
            CLASS_TO_ID_MODIFIER.removeModifications();
        }
    }
}