package net.beastcube.minigameapi.entityregistry;

/**
 * @author BeastCube
 */
public class EntityRegistrationException extends RuntimeException {

    public EntityRegistrationException() {
    }

    public EntityRegistrationException(String message) {
        super(message);
    }

    public EntityRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityRegistrationException(Throwable cause) {
        super(cause);
    }

    public EntityRegistrationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
