package net.beastcube.minigameapi;

import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.stats.Stat;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class RoundStatsCache {

    Map<UUID, Map<Stat, Long>> cache = new HashMap<>();

    public Optional<Map<Stat, Long>> getStats(UUID player) {
        return Optional.ofNullable(cache.get(player));
    }

    public Optional<Long> getStat(UUID player, Stat stat) {
        if (!cache.containsKey(player)) {
            return Optional.empty();
        }
        return Optional.ofNullable(cache.get(player).get(stat));
    }

    public void setStat(UUID player, Stat stat, long value) {
        if (!cache.containsKey(player)) {
            cache.put(player, new HashMap<>());
        }
        cache.get(player).put(stat, value);
    }

    public void increaseStat(UUID player, Stat stat, long value) {
        Optional<Long> old = getStat(player, stat);
        setStat(player, stat, old.isPresent() ? value + old.get() : value);
    }

    public void decreaseStat(UUID player, Stat stat, long value) {
        decreaseStat(player, stat, value, false);
    }

    public void decreaseStat(UUID player, Stat stat, long value, boolean canBecomeNegative) {
        Optional<Long> old = getStat(player, stat);
        long newvalue = old.isPresent() ? value - old.get() : value;
        setStat(player, stat, !canBecomeNegative && newvalue < 0 ? 0 : newvalue);
    }

    public void removePlayersCache(UUID player) {
        cache.remove(player);
    }

    public void clear() {
        cache.clear();
    }

    public void saveStats(UUID player) {
        if (player != null && cache.containsKey(player)) {
            cache.get(player).forEach((stat, value) -> MinigameType.BEDWARS.getStats().increaseStat(player, stat, value));
            cache.remove(player);
        }
    }

}
