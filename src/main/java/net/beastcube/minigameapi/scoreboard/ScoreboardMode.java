package net.beastcube.minigameapi.scoreboard;

/**
 * @author BeastCube
 */
public enum ScoreboardMode {
    IGNORE,
    NO_SCOREBOARD,
    JUST_TEAMS,
    TEAMS_WITH_NO_PLAYERS,
    TEAMS_WITH_PLAYERS,
    PLAYERS_WITH_SCORE
}
