package net.beastcube.minigameapi.scoreboard;

import net.beastcube.api.bukkit.util.scoreboard.SimpleScoreboard;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.maps.MapIcon;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 *         <p>
 *         Manages the scoreboard, builds it and displays it to the players. Using the scoreboardMode you can tell it how to build the scoreboard.
 */
public class ScoreboardManager {

    private static List<Team> Teams = new ArrayList<Team>();

    private static SimpleScoreboard buildScoreboard(Player p) {
        SimpleScoreboard simpleScoreboard = new SimpleScoreboard(ChatColor.DARK_PURPLE + MinigameAPI.getInstance().getMinigameType().getDisplayName());
        switch (MinigameAPI.getInstance().getState()) {
            case INGAME:
            case ARENA_COUNTDOWN:
                switch (MinigameAPI.getInstance().getScoreboardMode()) {
                    case NO_SCOREBOARD:
                        //return to not build the scoreboard
                        return simpleScoreboard;
                    case JUST_TEAMS:
                        for (int i = 0; i < MinigameAPI.getInstance().getNumberOfTeams(); i++)
                            simpleScoreboard.setLine(MinigameAPI.getInstance().getTeam(i).getScoreboardDisplayName(), MinigameAPI.getInstance().getTeam(i).getTeamScore());
                        break;
                    case TEAMS_WITH_NO_PLAYERS:
                        for (int i = 0; i < MinigameAPI.getInstance().getNumberOfTeams(); i++) {
                            simpleScoreboard.setLine(MinigameAPI.getInstance().getTeam(i).getScoreboardDisplayName(), MinigameAPI.getInstance().getTeam(i).getNumberOfPlayersWithState(PlayerState.INGAME));
                        }
                        break;
                    case TEAMS_WITH_PLAYERS:
                        for (int i = 0; i < MinigameAPI.getInstance().getNumberOfTeams(); i++) {
                            simpleScoreboard.setLine(MinigameAPI.getInstance().getTeam(i).getScoreboardDisplayName());
                            for (Player _p : MinigameAPI.getInstance().getTeam(i).getPlayers()) {
                                simpleScoreboard.setLine("  " + MinigameAPI.getInstance().getTeam(i).getChatColor() + _p.getDisplayName(), MinigameAPI.getInstance().getTeam(i).getTeamScore());
                            }
                        }
                        break;
                    case PLAYERS_WITH_SCORE:
                        for (Player _p : MinigameAPI.getInstance().getOnlinePlayers()) {
                            simpleScoreboard.setLine(_p.getDisplayName());
                        }
                        break;
                }
                break;
            case WAITING_FOR_PLAYERS:
            case LOBBY_COUNTDOWN_1:
            case LOBBY_COUNTDOWN_2:
                simpleScoreboard.setLine(" ");
                simpleScoreboard.setLine(ChatColor.GOLD + "Maps: ");
                for (MapIcon s : MapChoosing.getSuggestedMaps()) {
                    String map = s.getArena().getName();
                    int i = MapChoosing.getVotes(s.getArena());
                    simpleScoreboard.setLine(s.getArena().getName(), i);
                }
                //TODO: display stats
                break;
            case RESTARTING:
                simpleScoreboard.setLine(MinigameAPI.getInstance().getMinigameType().getDisplayName() + ChatColor.RED + " - Restarting");
                break;
            case DEVELOPMENT:
                simpleScoreboard.setLine(MinigameAPI.getInstance().getMinigameType().getDisplayName() + " - Development");
                break;
        }

        simpleScoreboard.build();

        registerTeams(simpleScoreboard);
        return simpleScoreboard;
    }

    public static void updateScoreboard(Player p) {
        if (MinigameAPI.getInstance().getScoreboardMode() != ScoreboardMode.IGNORE)
            buildScoreboard(p).send(p);
    }

    public static void updateScoreboard() {
        if (MinigameAPI.getInstance().getScoreboardMode() == ScoreboardMode.NO_SCOREBOARD || MinigameAPI.getInstance().getScoreboardMode() == ScoreboardMode.IGNORE)
            return;
        MinigameAPI.getInstance().getOnlinePlayers().forEach(ScoreboardManager::updateScoreboard);
    }

    private static SimpleScoreboard registerTeams(SimpleScoreboard scoreboard) {
        for (Team t : Teams) {
            registerScoreboardTeam(t, scoreboard.getScoreboard());
        }
        return scoreboard;
    }

    private static void registerScoreboardTeam(Team team, Scoreboard scoreboard) {
        org.bukkit.scoreboard.Team scoreboardTeam = scoreboard.getTeam(team.getName());
        if (scoreboardTeam == null)
            scoreboardTeam = scoreboard.registerNewTeam(team.getName());
        scoreboardTeam.setAllowFriendlyFire(MinigameAPI.getInstance().getDefaults().isPlayerDamage() && MinigameAPI.getInstance().getDefaults().isFriendlyFire());
        scoreboardTeam.setPrefix(team.getChatColor() + "");
        for (String p : scoreboardTeam.getEntries())
            scoreboardTeam.removeEntry(p);
        for (Player p : team.getPlayers())
            scoreboardTeam.addEntry(p.getName());
    }

    public static void registerTeam(Team team) {
        Teams.add(team);
    }

    public static void unregisterTeam(Team team) {
        Teams.remove(team);
    }

}
