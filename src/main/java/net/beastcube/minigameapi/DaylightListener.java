package net.beastcube.minigameapi;

import net.beastcube.api.commons.minigames.MinigameState;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;

/**
 * @author BeastCube
 *         <p>
 *         As we can not cancel the time to change directly, this class listens and sets the time back after a few ticks
 */
class DaylightListener {

    private static HashMap<World, Long> initialTimes = new HashMap<World, Long>();

    public static void setTime(World world, long time) {
        world.setTime(time);
        initialTimes.put(world, time);
    }

    public static long getTime(World world) {
        return initialTimes.get(world);
    }

    public static void startListening() {
        //reset the time after some ticks to make the daylight look steady
        Bukkit.getScheduler().scheduleSyncRepeatingTask(MinigameAPI.getInstance().getMinigame(), () -> {
            if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
                if (!MinigameAPI.getInstance().getDefaults().isDaylightCycle()) {
                    for (Map.Entry<World, Long> entry : initialTimes.entrySet()) {
                        entry.getKey().setFullTime(entry.getValue());
                    }
                }
            } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
                for (Map.Entry<World, Long> entry : initialTimes.entrySet()) {
                    entry.getKey().setFullTime(entry.getValue());
                }
            }
        }, 96, 96);
    }
}
