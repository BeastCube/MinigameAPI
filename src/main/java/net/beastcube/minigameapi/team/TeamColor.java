package net.beastcube.minigameapi.team;

import org.bukkit.Color;

/**
 * @author BeastCube
 */
public final class TeamColor {
    private static final Color[] colors =
            {
                    Color.BLUE,
                    Color.fromRGB(60, 170, 30),
                    Color.RED,
                    Color.YELLOW,
                    Color.PURPLE,
                    Color.ORANGE,
                    Color.AQUA,
                    Color.LIME
            };

    private static final String[] names =
            {
                    "Blau",
                    "Grün",
                    "Rot",
                    "Gelb",
                    "Lila",
                    "Orange",
                    "Aqua",
                    "Limette"
            };

    public static int getNumberOfSets() {
        return names.length;
    }

    public static String getName(int index) {
        return names[index];
    }

    public static Color getColor(int index) {
        return colors[index];
    }
}
