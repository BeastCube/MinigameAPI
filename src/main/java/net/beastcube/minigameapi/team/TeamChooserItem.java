package net.beastcube.minigameapi.team;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 *         <p>
 *         The item displayed in the team chooser menu
 */
public class TeamChooserItem extends MenuItem {

    Team team;

    private static List<String> getTeamLores(Team team) {
        List<String> lores = new ArrayList<String>();
        if (team != null) {
            for (int i = 0; i < team.getPlayers().size(); i++) {
                lores.add(team.getChatColor() + team.getPlayers().get(i).getName());
            }
        }
        return lores;
    }

    @SuppressWarnings("deprecation")
    public TeamChooserItem(Team team) {
        super(team.getDisplayName(), new ItemStack(Material.WOOL, team.getPlayers().size(), ColorConverter.colorToDyeColor(team.getColor()).getWoolData()));
        this.team = team;
    }

    @SuppressWarnings("deprecation")
    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        ItemStack stack = new ItemStack(Material.WOOL, team.getPlayers().size(), ColorConverter.colorToDyeColor(team.getColor()).getWoolData());
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(team.getDisplayName());
        meta.setLore(getTeamLores(team));
        stack.setItemMeta(meta);
        return stack;
    }

    private boolean mayJoin(Team team, Team from) {
        List<Team> teams = MinigameAPI.getInstance().getTeams();

        int rest = MinigameAPI.getInstance().getPlayersWithoutTeam().size() - 1;
        for (Team t : teams) {
            if (t != team) {
                if (t == from)
                    rest -= team.getNumberOfPlayers() - t.getNumberOfPlayers() + 1;
                else
                    rest -= team.getNumberOfPlayers() - t.getNumberOfPlayers();
                if (rest < 0)
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        e.setAction(ItemClickEvent.ItemClickAction.CLOSE);

        Player p = e.getPlayer();
        Team te = MinigameAPI.getInstance().getPlayersTeam(p);
        if (mayJoin(team, te)) {
            if (te != null)
                te.removePlayer(p);
            team.addPlayer(p);
            p.sendMessage(ChatColor.GREEN + "Du bist erfolgreich dem Team " + team.getChatColor() + team.getName() + ChatColor.GREEN + " beigetreten.");
            ScoreboardManager.updateScoreboard();
        } else {
            p.sendMessage(ChatColor.RED + "Es sind nicht genug Spieler online!");
        }
        TeamChoosing.update();
    }
}