package net.beastcube.minigameapi.team;

import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.events.TeamPlayerCountChangedEvent;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 *         <p>
 *         The team class. Contains all team specific data. This class can and should be overriden if there is any more
 *         data the minigame has to store for each team
 */
public class Team {
    //The players in this team
    private ArrayList<Player> players = new ArrayList<Player>();
    //The name of the team
    @Serialize
    private String name;
    //The color of the team
    @Serialize
    private Color color;
    //The current teamscore
    private int teamScore = 0;

    private Team() {
        //This constructor is needed for the serializer to work
    }

    public Team(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    //------------------------getters and setters--------------------------//
    public int getTeamScore() {
        return teamScore;
    }

    public void setTeamScore(int teamScore) {
        this.teamScore = teamScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatColor getChatColor() {
        return ColorConverter.colorToChatColor(color);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getDisplayName() {
        //teams that have lost should be gray, teams that have won will blink gray/gold.
        return (hasLost() ? ChatColor.GRAY : (hasWon() ? ((MinecraftServer.currentTick % 40 > 20) ? ChatColor.GOLD : ChatColor.GRAY) : ColorConverter.colorToChatColor(color))) + name;
    }

    public String getScoreboardDisplayName() {
        return getDisplayName();
    }

    public List<Player> getPlayers() {
        List<Player> result = new ArrayList<Player>();
        result.addAll(players);
        return result;
    }

    public void addPlayer(Player player) {
        players.add(player);
        ScoreboardManager.updateScoreboard();
    }

    public void removePlayer(Player player) {
        players.remove(player);
        TeamPlayerCountChangedEvent event = new TeamPlayerCountChangedEvent(this, players.size());
        Bukkit.getPluginManager().callEvent(event);
        Team ct;
        for (Player p : MinigameAPI.getInstance().getOnlinePlayers()) {
            ct = MinigameAPI.getInstance().getPlayersTeam(p);
            if (event.getMessageToTeam() != null) {
                if (ct == this) {
                    for (String s : event.getMessageToTeam().split("\\\\n"))
                        p.sendMessage(s);
                } else if (event.getBroadcastMessage() != null) {
                    for (String s : event.getBroadcastMessage().split("\\\\n"))
                        p.sendMessage(s);
                }
            } else if (event.getBroadcastMessage() != null) {
                for (String s : event.getBroadcastMessage().split("\\\\n"))
                    p.sendMessage(s);
            }

            if (event.getTitleMessage() != null) {
                if (ct == this)
                    event.getTitleMessage().send(p);
                else if (event.getBroadcastTitleMessage() != null)
                    event.getBroadcastTitleMessage().send(p);
            } else if (event.getBroadcastTitleMessage() != null) {
                event.getBroadcastTitleMessage().send(p);
            }
        }
        ScoreboardManager.updateScoreboard();
    }

    public void clearPlayers() {
        players.clear();
        ScoreboardManager.updateScoreboard();
    }

    public boolean containsPlayer(Player player) {
        return players.contains(player);
    }

    public int getNumberOfPlayers() {
        return players.size();
    }
    //-----------------------------------------------------------------------//

    public boolean hasState(PlayerState state) {
        if (this.getNumberOfPlayers() == 0)
            return false;
        for (Player p : this.getPlayers()) {
            if (MinigameAPI.getInstance().getPlayerState(p) != state)
                return false;
        }
        return true;
    }

    public boolean hasLost() {
        return hasState(PlayerState.LOST);
    }

    public boolean hasWon() {
        return hasState(PlayerState.WON);
    }

    public int getNumberOfPlayersWithState(PlayerState state) {
        int c = 0;
        for (Player p : this.getPlayers()) {
            if (MinigameAPI.getInstance().getPlayerState(p) == state)
                c++;
        }
        return c;
    }

    public boolean isIngame() {
        return hasState(PlayerState.INGAME);
    }

}
