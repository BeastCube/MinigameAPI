package net.beastcube.minigameapi.team;

import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author BeastCube
 *         <p>
 *         This class implements teamchat.
 */
public class TeamChatListener implements Listener {

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e) {
        final Player p = e.getPlayer();
        ChatColor color = ChatColor.GRAY;
        String prefix = "";
        String message = e.getMessage();
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            Team team = MinigameAPI.getInstance().getPlayersTeam(p);
            if (!MinigameAPI.getInstance().isIngame(p)) {
                //Spectator
                e.getRecipients().clear();
                e.getRecipients().addAll(MinigameAPI.getInstance().getNotPlayingPlayers());
            }
            if (team == null) {
                color = ChatColor.GRAY;
            } else {
                color = team.getChatColor();
                if (MinigameAPI.getInstance().getDefaults().isTeamChat()) {
                    if (e.getMessage().startsWith("@")) {
                        //Global
                        prefix = ChatColor.GOLD + "[GLOBAL] " + ChatColor.RESET;
                        message = message.substring(1, message.length());
                    } else {
                        //TeamChat
                        e.getRecipients().clear();
                        e.getRecipients().addAll(team.getPlayers());
                    }
                }
            }
        } else if (MinigameAPI.getInstance().getState() == MinigameState.RESTARTING) {
            prefix = ChatColor.DARK_RED + "[Spielende] ";
        }
        e.setFormat(prefix + color + p.getName() + ChatColor.DARK_GRAY + " » " + ChatColor.WHITE + message);
    }
}
