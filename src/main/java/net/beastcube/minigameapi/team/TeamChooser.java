package net.beastcube.minigameapi.team;

import org.bukkit.entity.Player;

/**
 * @author BeastCube
 *         <p>
 *         An interface for team choosers as there can be different team choosing ways for each minigame (via commands,
 *         via an inventoryItem, statues ...)
 */
public interface TeamChooser {
    void update();

    void refresh();

    void setTeams();

    void giveToPlayer(Player player);
}
