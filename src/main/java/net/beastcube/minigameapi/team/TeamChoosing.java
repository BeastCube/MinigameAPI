package net.beastcube.minigameapi.team;

import org.bukkit.entity.Player;

/**
 * @author BeastCube
 *         <p>
 *         This class handles team choosing, the minigame can set the team chooser that should be used via setTeamChooser(TeamChooser)
 */
public final class TeamChoosing {
    private static TeamChooser teamChooser;

    public static void setTeamChooser(TeamChooser teamChooser) {
        if (teamChooser != null) {
            TeamChoosing.teamChooser = teamChooser;
            teamChooser.refresh();
        }
    }

    public static void update() {
        if (teamChooser != null)
            teamChooser.update();
    }

    public static void refresh() {
        if (teamChooser != null)
            teamChooser.refresh();
    }

    public static void setTeams() {
        if (teamChooser != null)
            teamChooser.setTeams();
    }

    public static void giveToPlayer(Player player) {
        if (teamChooser != null) {
            teamChooser.giveToPlayer(player);
        }
    }
}
