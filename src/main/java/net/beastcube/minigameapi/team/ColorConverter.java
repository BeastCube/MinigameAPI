package net.beastcube.minigameapi.team;


import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author BeastCube
 *         <p>
 *         A small class that offers static methods to translate ChatColor to DyeColor and vice versa
 */
public final class ColorConverter {

    private static Color[] chatColors = {
            Color.fromRGB(0, 0, 0),
            Color.fromRGB(0, 0, 170),
            Color.fromRGB(0, 170, 0),
            Color.fromRGB(0, 170, 170),
            Color.fromRGB(170, 0, 0),
            Color.fromRGB(170, 0, 170),
            Color.fromRGB(255, 170, 0),
            Color.fromRGB(170, 170, 170),
            Color.fromRGB(85, 85, 85),
            Color.fromRGB(85, 85, 255),
            Color.fromRGB(85, 255, 85),
            Color.fromRGB(85, 255, 255),
            Color.fromRGB(255, 85, 85),
            Color.fromRGB(255, 85, 255),
            Color.fromRGB(255, 255, 85),
            Color.fromRGB(255, 255, 255)
    };
    private static Color[] dyeColors = {
            DyeColor.WHITE.getColor(),
            DyeColor.ORANGE.getColor(),
            DyeColor.MAGENTA.getColor(),
            DyeColor.LIGHT_BLUE.getColor(),
            DyeColor.YELLOW.getColor(),
            DyeColor.LIME.getColor(),
            DyeColor.PINK.getColor(),
            DyeColor.GRAY.getColor(),
            DyeColor.SILVER.getColor(),
            DyeColor.CYAN.getColor(),
            DyeColor.PURPLE.getColor(),
            DyeColor.BLUE.getColor(),
            DyeColor.BROWN.getColor(),
            DyeColor.GREEN.getColor(),
            DyeColor.RED.getColor(),
            DyeColor.BLACK.getColor()

            /*
            Color.fromRGB(16777215),
            Color.fromRGB(14188339),
            Color.fromRGB(11685080),
            Color.fromRGB(6724056),
            Color.fromRGB(15066419),
            Color.fromRGB(8375321),
            Color.fromRGB(15892389),
            Color.fromRGB(5000268),
            Color.fromRGB(10066329),
            Color.fromRGB(5013401),
            Color.fromRGB(8339378),
            Color.fromRGB(3361970),
            Color.fromRGB(6704179),
            Color.fromRGB(6717235),
            Color.fromRGB(10040115),
            Color.fromRGB(1644825)*/
    };

    public static Color chatColorToColor(ChatColor c) {
        return chatColors[c.ordinal()];
    }

    public static Color dyeColorToColor(DyeColor c) {
        return dyeColors[c.ordinal()];
    }


    private static int colorDiff(Color c, Color c2) {
        return (c.getRed() - c2.getRed()) * (c.getRed() - c2.getRed()) +
                (c.getGreen() - c2.getGreen()) * (c.getGreen() - c2.getGreen()) +
                (c.getBlue() - c2.getBlue()) * (c.getBlue() - c2.getBlue());
    }

    private static Object getNextColor(Color color, Color[] ref, Class<? extends Enum> type) {
        int smallest = 0;
        float sd = colorDiff(ref[0], color);
        float d;
        for (int i = 1; i < 16; i++) {
            d = colorDiff(ref[i], color);
            if (d < sd) {
                sd = d;
                smallest = i;
            }
        }
        try {
            Method m = type.getMethod("values");
            return ((Object[]) m.invoke(null))[smallest];
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ChatColor colorToChatColor(Color color) {
        return (ChatColor) getNextColor(color, chatColors, ChatColor.class);
    }

    public static DyeColor colorToDyeColor(Color color) {
        return (DyeColor) getNextColor(color, dyeColors, DyeColor.class);
    }

    public static DyeColor chatColorToDyeColor(ChatColor color) {
        return colorToDyeColor(chatColorToColor(color));
    }

    public static ChatColor dyeColorToChatColor(DyeColor color) {
        return colorToChatColor(dyeColorToColor(color));
    }
}
