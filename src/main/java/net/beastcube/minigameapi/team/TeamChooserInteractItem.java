package net.beastcube.minigameapi.team;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

/**
 * @author BeastCube
 *         <p>
 *         A basic team chooser offered by the minigame api
 */
public class TeamChooserInteractItem extends InteractItem implements TeamChooser {
    public static ItemMenu menu;

    private static TeamChooserInteractItem instance;

    public static TeamChooserInteractItem getTeamChooser() {
        if (instance == null)
            instance = new TeamChooserInteractItem();
        return instance;
    }

    private TeamChooserInteractItem() {
        List<Team> teams = MinigameAPI.getInstance().getTeams();
        menu = new ItemMenu(ChatColor.GOLD + "Team auswählen", ItemMenu.Size.fit(teams.size()), MinigameAPI.getInstance().getMinigame());
        InteractItemManager.registerInteractItem(this);
        refresh();
    }

    @Override
    public Material getMaterial() {
        return Material.WOOL;
    }

    @Override
    public String getDisplayName() {
        return ChatColor.GOLD + "Team-Wähler";
    }

    @Override
    public List<String> getLore() {
        return Arrays.asList(
                ChatColor.DARK_PURPLE + "Rechtsklick, um ein Team zu wählen!");
    }

    @Override
    public void execute(final Player p, JavaPlugin plugin, Object data) {
        refresh();
        menu.open(p);
    }

    public void update() {
        refresh();
        MinigameAPI.getInstance().getOnlinePlayers().forEach(menu::update);
    }

    public void refresh() {
        List<Team> teams = MinigameAPI.getInstance().getTeams();
        menu.clear();
        for (int i = 0; i < teams.size(); i++) {
            menu.setItem(i, new TeamChooserItem(teams.get(i)));
        }
    }

    public void setTeams() {
        MinigameAPI.getInstance().getOnlinePlayers().stream().filter(p -> MinigameAPI.getInstance().getPlayersTeam(p) == null).forEach(p -> MinigameAPI.getInstance().setPlayersTeam(p, MinigameAPI.getInstance().getSmallestTeam()));
    }

    public void giveToPlayer(Player player) {
        player.getInventory().addItem(this.createItemStack());
    }

    @Override
    public boolean isItem(ItemStack stack) {
        if (stack != null) {
            if (stack.hasItemMeta()) {
                return (getDisplayName().equals(stack.getItemMeta().getDisplayName()));
            }
        }
        return false;
    }
}
