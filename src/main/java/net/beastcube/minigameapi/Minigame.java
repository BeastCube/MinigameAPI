package net.beastcube.minigameapi;

/**
 * @author BeastCube
 */

import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.scoreboard.ScoreboardMode;
import net.beastcube.minigameapi.team.Team;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Minigame {

    Class<? extends Arena> arena();

    Class<? extends Team> team();

    MinigameType minigameType();

    ScoreboardMode scoreboardMode();

    String worldGenerator() default "EmptyWorldGenerator";

}
