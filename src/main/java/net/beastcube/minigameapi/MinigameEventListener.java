package net.beastcube.minigameapi;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.events.SlaveConnectedEvent;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.util.PacketAPI;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.countdowns.Countdowns;
import net.beastcube.minigameapi.events.PlayerIngameDeathEvent;
import net.beastcube.minigameapi.events.PlayerIngameJoinEvent;
import net.beastcube.minigameapi.events.PlayerIngameQuitEvent;
import net.beastcube.minigameapi.kits.Kit;
import net.beastcube.minigameapi.kits.KitChoosing;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.network.Messenger;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import net.beastcube.minigameapi.team.Team;
import net.beastcube.minigameapi.team.TeamChoosing;
import net.beastcube.minigameapi.util.PlayerInventory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;

/**
 * @author BeastCube
 */
public class MinigameEventListener implements Listener {

    private HashMap<Player, PlayerInventory> respawnInventories = new HashMap<Player, PlayerInventory>();

    public MinigameEventListener() {
        DaylightListener.startListening();
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onWorldLoad(WorldLoadEvent e) {
        DaylightListener.setTime(e.getWorld(), e.getWorld().getTime());
    }

    private void clearPlayerInventory(final Player p) {
        p.getInventory().clear();
        ItemStack nothing = new ItemStack(Material.AIR);
        p.getInventory().setHelmet(nothing);
        p.getInventory().setChestplate(nothing);
        p.getInventory().setLeggings(nothing);
        p.getInventory().setBoots(nothing);

        Bukkit.getScheduler().runTask(MinigameAPI.getInstance().getMinigame(), () -> {
            ItemStack item = p.getItemInHand();
            p.setItemInHand(new ItemStack(Material.WOOD_SWORD));
            p.setItemInHand(item);
        });
    }

    private void assignPlayerInventory(final Player p, Inventory inventory, ItemStack[] armorContents) {
        p.getInventory().setContents(inventory.getContents());
        p.getInventory().setArmorContents(armorContents);

        Bukkit.getScheduler().runTask(MinigameAPI.getInstance().getMinigame(), () -> {
            ItemStack item = p.getItemInHand();
            p.setItemInHand(new ItemStack(Material.WOOD_SWORD));
            p.setItemInHand(item);
        });
    }

    //LOW priority so that events listened by the minigame itself are called afterwards. This allows the minigame to cancel the event even
    //if it wouldn't be cancelled due to the defaults.
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(final PlayerJoinEvent e) {
        MinigameAPI<Arena, Team, AnnotationConfiguration> mgapi = MinigameAPI.getInstance();
        try {

            //Heal player
            e.getPlayer().setHealth(20D);
            e.getPlayer().setFoodLevel(20);
            e.getPlayer().setWalkSpeed(0.2f);

            //If the game is already running
            if (mgapi.getState().isArena()) {

                final PlayerIngameJoinEvent event = new PlayerIngameJoinEvent(e);
                Bukkit.getPluginManager().callEvent(event);
                //Clear the inventory
                Bukkit.getScheduler().runTask(mgapi.getMinigame(), () -> assignPlayerInventory(e.getPlayer(), event.getInventory(), event.getInventoryArmorContents()));

                ScoreboardManager.updateScoreboard();

                if (mgapi.getDefaults().isJoinIngame()) {
                    e.getPlayer().setGameMode(mgapi.getDefaults().getDefaultGamemode());
                    mgapi.setPlayerData(e.getPlayer(), new PlayerData(PlayerState.INGAME));
                    e.getPlayer().teleport(mgapi.getArena().getSpawn(e.getPlayer()));
                    e.setJoinMessage(mgapi.getJoinMessage(e.getPlayer()));
                } else {
                    e.getPlayer().setGameMode(GameMode.SPECTATOR);
                    e.getPlayer().teleport(mgapi.getArena().getSpectatorSpawn());
                    e.setJoinMessage(mgapi.getJoinMessage(e.getPlayer()));
                    mgapi.setPlayerData(e.getPlayer(), new PlayerData(PlayerState.SPECTATOR));
                }
            }
            //else if we are still in the lobby
            else if (mgapi.getState().isLobby()) {
                try {

                    TeamChoosing.refresh();

                    //update the inventory
                    Bukkit.getScheduler().runTask(mgapi.getMinigame(), () -> {
                        clearPlayerInventory(e.getPlayer());
                        for (PotionEffect effect : e.getPlayer().getActivePotionEffects())
                            e.getPlayer().removePotionEffect(effect.getType());
                        e.getPlayer().getInventory().addItem(InteractItemManager.PERSONALCHEST.createItemStack());
                        MapChoosing.giveToPlayer(e.getPlayer());
                        TeamChoosing.giveToPlayer(e.getPlayer());
                        KitChoosing.giveToPlayer(e.getPlayer());
                    });

                    e.getPlayer().setGameMode(GameMode.SURVIVAL);
                    if (mgapi.getLobbySpawn().isPresent()) //TODO remove this, so the exception is called?
                        e.getPlayer().teleport(mgapi.getLobbySpawn().get());
                    e.setJoinMessage(mgapi.getJoinMessage(e.getPlayer()));
                    mgapi.setPlayerData(e.getPlayer(), new PlayerData(PlayerState.IN_LOBBY));
                    if (mgapi.getOnlinePlayers().size() >= mgapi.getMinPlayers()) {
                        if (mgapi.getState() == MinigameState.WAITING_FOR_PLAYERS)
                            Countdowns.startTimeline();
                    }
                    ScoreboardManager.updateScoreboard();
                    Bukkit.getScheduler().scheduleSyncDelayedTask(mgapi.getMinigame(), ScoreboardManager::updateScoreboard, 1);

                } catch (Exception ex) {
                    mgapi.switchToDevelopment();
                    ex.printStackTrace();
                    Bukkit.broadcastMessage(ChatColor.RED + "The lobby wasn't set up correctly");
                }
            } else {

                e.getPlayer().setGameMode(GameMode.CREATIVE);
                if (mgapi.getLobbySpawn().isPresent())
                    e.getPlayer().teleport(mgapi.getLobbySpawn().get());
                e.setJoinMessage(mgapi.getJoinMessage(e.getPlayer()));
                mgapi.setPlayerData(e.getPlayer(), new PlayerData(PlayerState.NONE));

                ScoreboardManager.updateScoreboard();
                Bukkit.getScheduler().scheduleSyncDelayedTask(mgapi.getMinigame(), ScoreboardManager::updateScoreboard, 1);
            }
        } catch (Exception ex) {
            mgapi.setPlayerData(e.getPlayer(), new PlayerData(PlayerState.NONE));
            ex.printStackTrace();
        }
        MinigameAPI.getInstance().displayStats(e.getPlayer());
        Messenger.sendMinigameState(false);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerQuit(final PlayerQuitEvent e) {
        e.setQuitMessage(MinigameAPI.getInstance().getQuitMessage(e.getPlayer()));
        if (MinigameAPI.getInstance().getState() == MinigameState.LOBBY_COUNTDOWN_1 || MinigameAPI.getInstance().getState() == MinigameState.LOBBY_COUNTDOWN_2) {
            MapChoosing.removeVote(e.getPlayer());

            //smaller or equal because it isn't updated yet
            if (MinigameAPI.getInstance().getOnlinePlayers().size() <= MinigameAPI.getInstance().getMinPlayers())
                Countdowns.stopCountdown(); //TODO not working?!?
            MinigameAPI.getInstance().setState(MinigameState.WAITING_FOR_PLAYERS);
        }

        MinigameAPI.getInstance().removePlayerData(e.getPlayer());
        Team t = MinigameAPI.getInstance().getPlayersTeam(e.getPlayer());
        if (t != null) {
            t.removePlayer(e.getPlayer());
        }

        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            PlayerIngameQuitEvent event = new PlayerIngameQuitEvent(e, MinigameAPI.getInstance().getPlayerData(e.getPlayer()), MinigameAPI.getInstance().getPlayersTeam(e.getPlayer()));
            Bukkit.getPluginManager().callEvent(event);
        }
        MinigameAPI.getInstance().removeStats(e.getPlayer());
        Bukkit.getScheduler().runTask(MinigameAPI.getInstance().getMinigame(), () -> Messenger.sendMinigameState(false));
        ScoreboardManager.updateScoreboard();
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerRespawn(final PlayerRespawnEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME || MinigameAPI.getInstance().getState() == MinigameState.RESTARTING) {
            e.setRespawnLocation(MinigameAPI.getInstance().getArena().getSpawn(e.getPlayer()));
            Bukkit.getScheduler().scheduleSyncDelayedTask(MinigameAPI.getInstance().getMinigame(), () -> {
                PlayerInventory inv = respawnInventories.get(e.getPlayer());
                if (inv != null) {
                    inv.apply(e.getPlayer());
                    e.getPlayer().updateInventory();
                }
            }, 1);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onEntityDamage(EntityDamageEvent e) {
        if (MinigameAPI.getInstance().getState() != MinigameState.INGAME) {
            if (e.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION && !MinigameAPI.getInstance().getDefaults().isTntDamage())
                e.setDamage(0);
            else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT)
                e.setCancelled(true);
        }
        if (e.getEntity() instanceof ItemFrame) {
            if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
                e.setCancelled(!MinigameAPI.getInstance().getDefaults().isHangingBreak());
            } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (e.getEntity() instanceof Player) {
                if (e.getDamager() instanceof Player) {
                    if (MinigameAPI.getInstance().getDefaults().isPlayerDamage() && (MinigameAPI.getInstance().getDefaults().isFriendlyFire() || MinigameAPI.getInstance().getPlayersTeam((Player) e.getDamager()) != MinigameAPI.getInstance().getPlayersTeam((Player) e.getEntity()))) {
                        MinigameAPI.getInstance().setLastDamager((Player) e.getEntity(), (Player) e.getDamager());
                    } else
                        e.setCancelled(true);
                } else if (e.getDamager() instanceof Arrow) {
                    Arrow arrow = (Arrow) e.getDamager();
                    if (arrow.getShooter() instanceof Player) {
                        MinigameAPI.getInstance().setLastDamager((Player) e.getEntity(), (Player) arrow.getShooter());
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onWeatherChange(WeatherChangeEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            e.setCancelled(!MinigameAPI.getInstance().getDefaults().isWeatherChange());
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }                                                                                  //

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockBreak(BlockBreakEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            e.setCancelled(!MinigameAPI.getInstance().getDefaults().isBlockBreaking());
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockPlace(BlockPlaceEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (e.getBlock().getType() == Material.FIRE) {
                e.setCancelled(!MinigameAPI.getInstance().getDefaults().isFlintAndSteel());
            } else {
                e.setCancelled(!MinigameAPI.getInstance().getDefaults().isBlockPlacing());
            }
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryOpen(InventoryOpenEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (!(e.getInventory().getHolder() instanceof Player)) {
                if (e.getInventory().getType() == InventoryType.CHEST || e.getInventory().getType() == InventoryType.ENDER_CHEST)
                    e.setCancelled(!MinigameAPI.getInstance().getDefaults().isContainerOpening());
            } else {
                if (e.getInventory().getType() == InventoryType.BREWING || e.getInventory().getType() == InventoryType.CRAFTING || e.getInventory().getType() == InventoryType.FURNACE)
                    e.setCancelled(!MinigameAPI.getInstance().getDefaults().isCrafting());
                else
                    e.setCancelled(!MinigameAPI.getInstance().getDefaults().isInventoryOpening());
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerGetHungry(FoodLevelChangeEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            e.setCancelled(!MinigameAPI.getInstance().getDefaults().isHunger());
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCraft(CraftItemEvent e) {
        e.setCancelled(!MinigameAPI.getInstance().getDefaults().isCrafting());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerMove(PlayerMoveEvent e) {
        if (e.getPlayer().getLocation().getY() < 0) {
            if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
                if (MinigameAPI.getInstance().getDefaults().isDieBeneathZero() && e.getPlayer().getHealth() > 0)
                    e.getPlayer().setHealth(0.0D);
            } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT)
                if (MinigameAPI.getInstance().getLobbySpawn().isPresent()) {
                    e.getPlayer().teleport(MinigameAPI.getInstance().getLobbySpawn().get());
                } else {
                    e.getPlayer().teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                }

        }
        //if (MinigameAPI.getInstance().getState() == MinigameState.ARENA_COUNTDOWN && e.getFrom().distanceSquared(e.getTo()) > 0)
        //    e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onEntityExplode(EntityExplodeEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (!MinigameAPI.getInstance().getDefaults().isBlockExploding())
                e.blockList().clear();
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            if (e.getEntityType() == EntityType.PRIMED_TNT)
                e.blockList().clear();
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerDeath(final PlayerDeathEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (!MinigameAPI.getInstance().getDefaults().isDropStuffOnDeath()) {
                e.getDrops().clear();
            }
            e.setKeepInventory(MinigameAPI.getInstance().getDefaults().isKeepInventoryOnDeath());

            Kit kit = MinigameAPI.getInstance().getPlayerKit(e.getEntity());
            PlayerInventory inv = kit == null ? new PlayerInventory() : kit.getPlayerInventory();
            PlayerIngameDeathEvent event = new PlayerIngameDeathEvent(e, inv);
            Bukkit.getPluginManager().callEvent(event);
            respawnInventories.put(e.getEntity(), event.getInventory());
            if (MinigameAPI.getInstance().getDefaults().isAutoRespawn()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(MinigameAPI.getInstance().getMinigame(), () -> {
                    try {
                        PacketAPI.sendRespawnPacket(e.getEntity());
                    } catch (Exception ex) {
                        MinigameAPI.getInstance().getMinigame().getLogger().info("[WARNING]: Could not send respawn-packet to player " + e.getEntity().getName());
                        MinigameAPI.getInstance().getMinigame().getLogger().info(ex.getClass().toString() + ": " + ex.getMessage());
                    }
                }, 1);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerEnterBed(PlayerBedEnterEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME && !MinigameAPI.getInstance().getDefaults().isEnterBedAllowed())
            e.setCancelled(true);
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent e) {
        // Physical means jump on it
        if (e.getAction() == Action.PHYSICAL) {
            Block block = e.getClickedBlock();
            if (block == null)
                return;
            // If the block is farmland (soil)
            if (block.getType() == Material.SOIL) {
                // Cancel event and set the block
                if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
                    if (!MinigameAPI.getInstance().getDefaults().isDestroySoil()) {
                        e.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
                        e.setCancelled(true);
                        block.setTypeIdAndData(block.getType().getId(), block.getData(), true);
                    }
                } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
                    e.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
                    e.setCancelled(true);
                    block.setTypeIdAndData(block.getType().getId(), block.getData(), true);
                }
            }
        } else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) { //TODO Falltüren können geöffnet werden -> Defaults doors?
            if (!(e.getPlayer().isSneaking() && e.isBlockInHand())) {
                if (e.getClickedBlock().getType() == Material.BED_BLOCK)
                    e.setCancelled(!MinigameAPI.getInstance().getDefaults().isEnterBedAllowed());
                else if (e.getClickedBlock().getType() == Material.WORKBENCH || e.getClickedBlock().getType() == Material.FURNACE || e.getClickedBlock().getType() == Material.BURNING_FURNACE ||
                        e.getClickedBlock().getType() == Material.BREWING_STAND || e.getClickedBlock().getType() == Material.ANVIL || e.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE)
                    e.setCancelled(!MinigameAPI.getInstance().getDefaults().isCrafting());
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onHangingBreak(HangingBreakEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (MinigameAPI.getInstance().getDefaults().isHangingBreak())
                e.setCancelled(true);
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockBurn(BlockBurnEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (!MinigameAPI.getInstance().getDefaults().isBlockBurning())
                e.setCancelled(true);
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        if (MinigameAPI.getInstance().getState() == MinigameState.INGAME) {
            if (!MinigameAPI.getInstance().getDefaults().isDropItems())
                e.setCancelled(true);
        } else if (MinigameAPI.getInstance().getState() != MinigameState.DEVELOPMENT) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onSlaveConnected(SlaveConnectedEvent e) {
        Messenger.sendMinigameState(false);
    }

}
