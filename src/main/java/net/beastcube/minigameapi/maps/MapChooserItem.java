package net.beastcube.minigameapi.maps;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 *         <p>
 *         The item that will be in the menu for each map the players can vote for
 */
public class MapChooserItem extends MenuItem {

    MapIcon map;

    public MapChooserItem(MapIcon icon) {
        super(ChatColor.GOLD + icon.getArena().getName(), icon.getStack(), "");
        map = icon;
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        return map.getStack();
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        MapChoosing.vote(map.getArena(), e.getPlayer());
        e.getPlayer().sendMessage(ChatColor.GREEN + "Du hast erfolgreich für die Map " + ChatColor.DARK_PURPLE + map.getArena().getName() + ChatColor.GREEN + " abgestimmt.");
        e.setAction(ItemClickEvent.ItemClickAction.CLOSE);
    }
}