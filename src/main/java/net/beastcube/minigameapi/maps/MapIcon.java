package net.beastcube.minigameapi.maps;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.minigameapi.arena.Arena;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 *         <p>
 *         A small class that brings a maps name and its icon together
 */
@AllArgsConstructor
public class MapIcon {
    @Getter
    private Arena arena;

    public ItemStack getStack() {
        return new ItemBuilder(arena.getIcon().clone()).amount(MapChoosing.getVotes(arena))
                .name(ChatColor.GOLD + arena.getName())
                .lore("").lore(ChatColor.GREEN + "" + MapChoosing.getVotes(arena) + " Stimmen").build();

    }

}
