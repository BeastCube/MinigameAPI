package net.beastcube.minigameapi.maps;


import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author BeastCube
 */
public interface MapValidator {
    boolean isValid(YamlConfiguration config);
}
