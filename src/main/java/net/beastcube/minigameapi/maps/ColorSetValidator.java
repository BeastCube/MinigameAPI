package net.beastcube.minigameapi.maps;

import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author BeastCube
 */
public class ColorSetValidator implements MapValidator {

    private int colorset;

    public ColorSetValidator(int colorset) {
        this.colorset = colorset;
    }

    public int getColorSet() {
        return colorset;
    }

    public void setColorSet(int colorset) {
        this.colorset = colorset;
    }

    @Override
    public boolean isValid(YamlConfiguration config) {
        int set = config.getInt("colorSet");
        return set == getColorSet();
    }
}
