package net.beastcube.minigameapi.maps;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * @author BeastCube
 *         <p>
 *         One possibility to make map voting possible, offered by the minigameapi.
 */
public class MapChooserInteractItem extends InteractItem implements MapChooser {

    private static MapChooserInteractItem instance;
    private static final int[][] slots = {{4}, {2, 6}, {1, 4, 7}};

    public static MapChooserInteractItem getMapChooser() {
        if (instance == null)
            instance = new MapChooserInteractItem();
        return instance;
    }

    public ItemMenu menu;
    private MapIcon[] maps;

    private MapChooserInteractItem() {
        menu = new ItemMenu("Map wählen", ItemMenu.Size.ONE_LINE, MinigameAPI.getInstance().getMinigame());
        InteractItemManager.registerInteractItem(this);

        updateMaps();
    }

    public void updateMaps() {
        List<Arena> allmaps = new ArrayList<>(MinigameAPI.getInstance().getMaps());
        int size = Math.min(allmaps.size(), 3);
        maps = new MapIcon[size];

        Random r = new Random();
        int j = 0;
        while (Math.min(allmaps.size(), 3 - j) >= 1) {
            int c = r.nextInt(allmaps.size());
            maps[j] = new MapIcon(allmaps.get(c));
            j++;
            allmaps.remove(c);
        }
    }

    public void update() {
        refresh();
        ScoreboardManager.updateScoreboard();
    }

    public void refresh() {
        menu.clear();

        for (int i = 0; i < maps.length; i++) {
            menu.setItem(slots[maps.length - 1][i], new MapChooserItem(maps[i]));
        }
    }

    public void giveToPlayer(Player player) {
        player.getInventory().addItem(this.createItemStack());
    }

    public void takeFromPlayer(Player player) {
        player.getInventory().remove(this.createItemStack());
    }

    public List<MapIcon> getSuggestedMaps() {
        return Arrays.asList(maps);
    }

    public void close() {
        MinigameAPI.getInstance().getOnlinePlayers().forEach(menu::close);
    }

    @Override
    public Material getMaterial() {
        return Material.COMPASS;
    }

    @Override
    public String getDisplayName() {
        return ChatColor.GOLD + "Map-Wähler";
    }

    @Override
    public List<String> getLore() {
        return Collections.singletonList(
                ChatColor.DARK_PURPLE + "Rechtsklick, um für eine Map zu voten!");
    }

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        menu.open(p);
    }

}
