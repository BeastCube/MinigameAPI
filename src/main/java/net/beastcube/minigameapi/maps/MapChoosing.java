package net.beastcube.minigameapi.maps;

import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.arena.Arena;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 *         <p>
 *         This final class handles all the map voting. Set the map chooser you want via setMapChooser(MapChooser)
 */
public final class MapChoosing {

    public static HashMap<Arena, List<UUID>> Voted = new HashMap<>();

    private static MapChooser mapChooser;
    private static MapValidator mapValidator = null;

    public static void init() {
        List<Arena> maps = MinigameAPI.getInstance().getMaps();
        for (Arena arena : maps)
            Voted.put(arena, new ArrayList<>());
    }

    public static void setMapChooser(MapChooser mapChooser) {
        if (mapChooser != null) {
            MapChoosing.mapChooser = mapChooser;
            mapChooser.updateMaps();
            mapChooser.refresh();
        }
    }

    public static void setMapValidator(MapValidator mapValidator) {
        MapChoosing.mapValidator = mapValidator;
        List<Arena> maps = MinigameAPI.getInstance().getMaps();
        Voted.clear();
        for (Arena arena : maps)
            Voted.put(arena, new ArrayList<>());
        mapChooser.updateMaps();
        refresh();
    }

    public static List<String> getMostWanted() {
        ArrayList<String> result = new ArrayList<>();
        int max = -1;
        int c;
        Set<Arena> maps = Voted.keySet();
        for (Arena s : maps) {
            c = Voted.get(s).size();
            if (c > max) {
                result.clear();
                result.add(s.getName());
                max = c;
            } else if (c == max) {
                result.add(s.getName());
            }
        }
        return result;
    }

    public static void vote(Arena map, Player player) {
        Arena current = getVote(player);
        List<UUID> m = Voted.get(map);
        if (current == null)
            m.add(player.getUniqueId());
        else if (!current.equals(map)) {
            Voted.get(current).remove(player.getUniqueId());
            m.add(player.getUniqueId());
        }
        if (mapChooser != null)
            mapChooser.update();
    }

    public static void addVote(Arena map, Player player) {
        Voted.get(map).add(player.getUniqueId());
        if (mapChooser != null)
            mapChooser.update();
    }

    public static void removeVote(Arena map, Player player) {
        if (map != null) {
            if (Voted.get(map).contains(player.getUniqueId())) {
                Voted.get(map).remove(player.getUniqueId());
                if (mapChooser != null)
                    mapChooser.update();
            }
        }
    }

    public static void removeVote(Player player) {
        removeVote(getVote(player), player);
    }

    public static int getVotes(Arena map) {
        for (Map.Entry<Arena, List<UUID>> s : Voted.entrySet()) {
            if (s.getKey().equals(map))
                return s.getValue().size();
        }
        return 0;
    }

    public static Arena getVote(Player player) {
        Collection<Arena> keys = Voted.keySet();
        for (Arena s : keys) {
            if (Voted.get(s).contains(player.getUniqueId()))
                return s;
        }
        return null;
    }

    public static void update() {
        if (mapChooser != null)
            mapChooser.update();
    }

    public static void refresh() {
        if (mapChooser != null)
            mapChooser.refresh();
    }

    public static void giveToPlayer(Player player) {
        if (mapChooser != null)
            mapChooser.giveToPlayer(player);
    }

    public static void close() {
        if (mapChooser != null)
            mapChooser.close();
    }

    public static void takeFromPlayer(Player player) {
        if (mapChooser != null)
            mapChooser.takeFromPlayer(player);
    }

    public static boolean isValid(YamlConfiguration config) {
        return mapValidator == null || mapValidator.isValid(config);
    }

    public static List<MapIcon> getSuggestedMaps() {
        if (mapChooser != null)
            return mapChooser.getSuggestedMaps();
        else
            return MinigameAPI.getInstance().getMaps().stream().map(MapIcon::new).collect(Collectors.toList());
    }

}
