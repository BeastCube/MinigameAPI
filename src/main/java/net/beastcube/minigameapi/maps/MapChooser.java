package net.beastcube.minigameapi.maps;

import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author BeastCube
 *         <p>
 *         An interface for map choosers as there can be many different ways to achieve map choosing.
 */
public interface MapChooser {

    void refresh();

    void update();

    void giveToPlayer(Player player);

    void takeFromPlayer(Player player);

    void close();

    void updateMaps();

    List<MapIcon> getSuggestedMaps();

}