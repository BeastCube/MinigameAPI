package net.beastcube.minigameapi;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlave;
import net.beastcube.api.bukkit.commands.CommandHandler;
import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.util.PlayerDirection;
import net.beastcube.api.bukkit.util.serialization.Serializer;
import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.api.commons.Providers;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.players.Coins;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.commons.stats.minigames.BedwarsStats;
import net.beastcube.api.commons.util.LoadingIndicator;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.countdowns.Countdowns;
import net.beastcube.minigameapi.defaults.Defaults;
import net.beastcube.minigameapi.defaults.PlayerAmountBounds;
import net.beastcube.minigameapi.events.*;
import net.beastcube.minigameapi.kits.Kit;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.network.IncomingMessageHandler;
import net.beastcube.minigameapi.network.Messenger;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import net.beastcube.minigameapi.scoreboard.ScoreboardMode;
import net.beastcube.minigameapi.team.Team;
import net.beastcube.minigameapi.team.TeamChatListener;
import net.beastcube.minigameapi.util.IO;
import net.beastcube.minigameapi.util.Util;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 *         <p>
 *         A final API to access current minigame state, player states, team states... and directly
 *         set options like playerDamage, weatherChange... via the Defaults
 */
@SuppressWarnings("unused")
public final class MinigameAPI<A extends Arena, T extends Team, C extends AnnotationConfiguration> implements Listener {
    @Getter
    private static MinigameAPI<Arena, Team, AnnotationConfiguration> instance;
    @Getter
    private RoundStatsCache roundStatsCache = new RoundStatsCache();
    @Getter
    private JavaPlugin minigame;
    @Getter
    private MinigameType minigameType;
    @Getter
    private ScoreboardMode scoreboardMode;
    @Getter
    private PlayerAmountBounds playerAmountBounds;
    @Getter
    private Defaults defaults;
    @Getter
    private Config config;
    //These methods should be called if A or Team are extended.
    @Getter
    private Class<A> arenaClass;
    @Getter
    private Class<T> teamClass;
    @Getter
    @Setter
    private String joinMessage = ChatColor.GRAY + "[" + ChatColor.GREEN + "+" + ChatColor.GRAY + "] %s";
    @Getter
    private CommandHandler commandHandler;
    @Getter
    private List<A> maps = new ArrayList<>();

    public String getJoinMessage(Player p) {
        return String.format(joinMessage, p.getName());
    }

    @Getter
    @Setter
    private String quitMessage = ChatColor.GRAY + "[" + ChatColor.RED + "-" + ChatColor.GRAY + "] %s";

    public String getQuitMessage(Player p) {
        return String.format(quitMessage, p.getName());
    }

    @Getter
    private MinigameState state;
    private final HashMap<Player, Float> walkSpeed = new HashMap<>();
    @Getter
    private LobbySettings lobbySettings = new LobbySettings();
    private Map<UUID, List<Hologram>> statsHolograms = new HashMap<>();

    public boolean setState(MinigameState state) {
        GameStateChangedEvent event = new GameStateChangedEvent(state);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            if (state == MinigameState.ARENA_COUNTDOWN) {
                for (Player p : getOnlinePlayers()) {
                    walkSpeed.put(p, p.getWalkSpeed());
                    p.setWalkSpeed(0.001f); //TODO
                }
            } else if (this.state == MinigameState.ARENA_COUNTDOWN) {
                for (Player p : getOnlinePlayers()) {
                    Float f = walkSpeed.get(p);
                    if (f != null)
                        p.setWalkSpeed(f);
                }
            }
            this.state = state;
            ScoreboardManager.updateScoreboard();
            Messenger.sendMinigameState(false);
            return true;
        }
        return false;
    }

    public boolean increaseState() {
        return setState(MinigameState.values()[getState().ordinal() + 1]);
    }

    @Getter
    private List<Team> teams = new ArrayList<>();

    public Team getTeam(int index) {
        return teams.get(index);
    }

    public Team getTeam(String name) {
        for (Team t : teams)
            if (t.getName().equals(name))
                return t;
        return null;
    }

    public Team getPlayersTeam(Player player) {
        if (teams == null)
            return null;
        for (Team t : teams) {
            for (Player p : t.getPlayers()) {
                if (p == player)
                    return t;
            }
        }
        return null;
    }

    public void setPlayersTeam(Player player, Team team) {
        Team previous = getPlayersTeam(player);
        if (previous != null) {
            previous.removePlayer(player);
        }
        if (team != null)
            team.addPlayer(player);
    }

    public int getNumberOfTeams() {
        return teams.size();
    }

    public void addTeam(Team team) {
        teams.add(team);
        ScoreboardManager.registerTeam(team);
    }

    public void removeTeam(Team team) {
        teams.remove(team);
        ScoreboardManager.unregisterTeam(team);
    }

    public Team getSmallestTeam() {
        if (teams == null) {
            return null;
        }
        int c = Integer.MAX_VALUE;
        Team smallest = null;
        for (Team t : teams) {
            if (t.getNumberOfPlayers() < c) {
                smallest = t;
                c = smallest.getNumberOfPlayers();
            }
        }
        return smallest;
    }

    public List<Player> getPlayersWithoutTeam() {
        return getOnlinePlayers().stream().filter(p -> getPlayersTeam(p) == null).collect(Collectors.toList());
    }

    //Whenever a player joins a default playerData will be added to this map. This will prevent the data from being null.
    private Map<Player, PlayerData> playerData = new HashMap<>();

    private void updateGameMode(Player player, PlayerState state) {
        if (state.isOutOfGame()) {
            player.setFoodLevel(20);
            player.setHealth(20);
            player.setFireTicks(0);
            player.setGameMode(GameMode.SPECTATOR);
            player.setFlying(true);
            player.teleport(getArena().getSpectatorSpawn());
        } else {
            player.setGameMode(getDefaults().getDefaultGamemode());
        }
    }

    private void callPlayerStateChangedEvent(PlayerState state, Player player) {
        PlayerStateChangedEvent event = new PlayerStateChangedEvent(state, player);
        Bukkit.getPluginManager().callEvent(event);
        for (Player p : getOnlinePlayers()) {
            if (p == player) {
                if (event.getMessage() != null)
                    for (String s : event.getMessage().split("\\\\n"))
                        p.sendMessage(s);
            } else if (event.getBroadcastMessage() != null)
                for (String s : event.getBroadcastMessage().split("\\\\n"))
                    p.sendMessage(s);
        }
        if (state.isOutOfGame()) {
            saveStats(player);
        }
    }

    private void callTeamStateChangedEvent(PlayerState state, Team team) {
        TeamStateChangedEvent event = new TeamStateChangedEvent(state, team);
        Bukkit.getPluginManager().callEvent(event);
        Team ct;
        for (Player p : getOnlinePlayers()) {
            ct = getPlayersTeam(p);
            if (event.getMessage() != null) {
                if (ct == team) {
                    for (String s : event.getMessage().split("\\\\n"))
                        p.sendMessage(s);
                } else if (event.getBroadcastMessage() != null) {
                    for (String s : event.getBroadcastMessage().split("\\\\n"))
                        p.sendMessage(s);
                }
            } else if (event.getBroadcastMessage() != null) {
                for (String s : event.getBroadcastMessage().split("\\\\n"))
                    p.sendMessage(s);
            }

            if (event.getTitleMessage() != null) {
                if (ct == team)
                    event.getTitleMessage().send(p);
                else if (event.getBroadcastTitleMessage() != null)
                    event.getBroadcastTitleMessage().send(p);
            } else if (event.getBroadcastTitleMessage() != null) {
                event.getBroadcastTitleMessage().send(p);
            }
        }
    }

    /**
     * The same as Bukkit.getOnlinePlayers except during the PlayerQuitEvent, the PlayerIngameQuitEvent and the TeamCountChangeEvent.
     * While Bukkit.getOnlinePlayers is not updated then, MinigameAPI.getOnlinePlayers will be.
     */
    public Set<Player> getOnlinePlayers() {
        return playerData.keySet();
    }

    public void setPlayerState(Player player, PlayerState state) {
        PlayerData data = playerData.get(player);
        if (data.getState() != state) {
            data.setState(state);
            updateGameMode(player, state);
            callPlayerStateChangedEvent(state, player);
            Team t = getPlayersTeam(player);
            if (t != null) {
                if (t.hasState(state)) {
                    callTeamStateChangedEvent(state, t);
                }
            }
        }
        ScoreboardManager.updateScoreboard();
    }

    public PlayerState getPlayerState(Player player) {
        return playerData.get(player).getState();
    }

    public void setPlayerScore(Player player, int score) {
        playerData.get(player).setScore(score);
    }

    public int getPlayerScore(Player player) {
        return playerData.get(player).getScore();
    }

    public void setPlayerKit(Player player, Kit kit) {
        playerData.get(player).setKit(kit);
    }

    public Kit getPlayerKit(Player player) {
        return playerData.get(player).getKit();
    }

    public void setPlayerData(Player player, PlayerData data) {
        PlayerData pdata = playerData.get(player);
        boolean hasStateChanged = pdata == null || pdata.getState() != data.getState();
        if (pdata == null) {
            pdata = new PlayerData(data.getState());
            playerData.put(player, pdata);
        } else if (pdata.getState() != data.getState()) {
            pdata.setState(data.getState());
        }
        pdata.setKit(data.getKit());
        pdata.setScore(data.getScore());
        if (hasStateChanged) {
            updateGameMode(player, data.getState());
            callPlayerStateChangedEvent(data.getState(), player);
            Team t = getPlayersTeam(player);
            if (t != null) {
                if (t.hasState(data.getState())) {
                    callTeamStateChangedEvent(data.getState(), t);
                }
            }
        }
        ScoreboardManager.updateScoreboard();
    }

    public PlayerData getPlayerData(Player player) {
        return playerData.get(player);
    }

    public void removePlayerData(Player player) {
        playerData.remove(player);
    }

    public Player getLastDamager(Player player) {
        return playerData.get(player).getLastDamager();
    }

    public void setLastDamager(Player player, Player damager) {
        playerData.get(player).setLastDamager(damager);
    }
    //Just some nicer ways of accessing the data

    /**
     * @return whether the player is currently ingame.
     */
    public boolean isIngame(Player player) {
        return getPlayerState(player) == PlayerState.INGAME;
    }

    /**
     * @return whether the player has already won.
     */
    public boolean hasWon(Player player) {
        return getPlayerState(player) == PlayerState.WON;
    }

    /**
     * @return whether the player has already lost.
     */
    public boolean hasLost(Player player) {
        return getPlayerState(player) == PlayerState.LOST;
    }

    /**
     * @return whether the player has joined as spectator. Important: Players that have lost or won are not counted as spectators!
     */
    public boolean isJustSpectator(Player player) {
        return getPlayerState(player) == PlayerState.SPECTATOR;
    }

    /**
     * @return all players that are currently ingame.
     */
    public List<Player> getPlayersIngame() {
        return getOnlinePlayers().stream().filter(this::isIngame).collect(Collectors.toList());
    }

    /**
     * @return all players that have already won
     */
    public List<Player> getPlayersWon() {
        return getOnlinePlayers().stream().filter(this::hasWon).collect(Collectors.toList());
    }

    /**
     * @return all players that have already lost
     */
    public List<Player> getPlayersLost() {
        return getOnlinePlayers().stream().filter(this::hasLost).collect(Collectors.toList());
    }

    /**
     * @return all players that have joined as spectators
     */
    public List<Player> getSpectators() {
        return getOnlinePlayers().stream().filter(this::isJustSpectator).collect(Collectors.toList());
    }

    /**
     * @return all players that have either won, lost or already joined as spectators
     */
    public List<Player> getNotPlayingPlayers() {
        return getOnlinePlayers().stream().filter(p -> hasWon(p) || hasLost(p) || isJustSpectator(p)).collect(Collectors.toList());
    }

    /**
     * @return all players that have IN_LOBBY as PlayerState
     */
    public List<Player> getPlayersInLobby() {
        return getOnlinePlayers().stream().filter(p -> getPlayerState(p) == PlayerState.IN_LOBBY).collect(Collectors.toList());
    }

    /**
     * @param state the state the players have to have
     * @return returns all players with a specific state
     */
    public List<Player> getPlayersWithState(PlayerState state) {
        return getOnlinePlayers().stream().filter(p -> getPlayerState(p) == state).collect(Collectors.toList());
    }

    /**
     * @return all teams that have at least one player that is still ingame
     */
    public List<Team> getTeamsIngame() {
        List<Team> teamsIngame = new ArrayList<>();
        if (getTeams() != null) {
            teamsIngame.addAll(getTeams().stream().filter(Team::isIngame).collect(Collectors.toList()));
        }
        return teamsIngame;
    }

    public Optional<Location> getLobbySpawn() {
        return Optional.ofNullable(lobbySettings.getSpawn());
    }

    public void setLobbySpawn(Location lobbySpawn) {
        lobbySettings.setSpawn(lobbySpawn);
    }

    private A arena;

    public A getArena() {
        return arena;
    }

    public void setArena(A arena) {
        this.arena = arena;
        commandHandler.registerCommands(arena);
    }

    public void setArena(String name) {
        try {
            IO.copyMap(getArenaFolder(name), new File(Bukkit.getWorldContainer(), name));
            World world = IO.loadWorld(name, getArenaFolder(name));
            File file = new File(Bukkit.getWorldContainer(), name + "/arena.json");
            setArena(loadArena(file, name, arenaClass));
            arena.setWorld(world);
            Bukkit.getPluginManager().callEvent(new ArenaLoadedEvent(arena, world));
        } catch (InstantiationException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    public File getArenasFolder() {
        return new File(config.worldsFolder, minigameType.name());
    }

    public File getArenaFolder(String name) {
        return new File(getArenasFolder(), name);
    }

    public static <A extends Arena> A loadArena(File file, String name, Class<A> arenaClass) throws IOException, IllegalAccessException, InstantiationException {
        A arena;
        if (file.exists()) {
            String s = FileUtils.readFileToString(file, "UTF-8");
            if (!s.isEmpty() && !StringUtils.isWhitespace(s)) {
                arena = Serializer.deserialize(s, arenaClass);
            } else {
                arena = arenaClass.newInstance(); //Empty file
            }
        } else {
            arena = arenaClass.newInstance();
        }
        arena.setName(name);
        return arena;
    }

    private void loadMaps() {
        String worlds = this.getConfig().worldsFolder;
        if (worlds != null) {
            File path = new File((worlds.endsWith(File.separator) ? worlds : worlds + File.separator) + minigameType.name());
            File[] files = path.listFiles();
            File arenaFile;
            if (files != null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        arenaFile = new File(f.getAbsolutePath() + "/arena.json");
                        if (arenaFile.exists()) {
                            try {
                                maps.add(loadArena(arenaFile, f.getName(), arenaClass));
                            } catch (IOException | IllegalAccessException | InstantiationException e) {
                                minigame.getLogger().warning("Fehler beim Laden der A: " + f.getName());
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    public int getMinPlayers() {
        switch (getPlayerAmountBounds().getType()) {
            case MIN_MAX:
            case MIN:
                return getPlayerAmountBounds().getMin();
            case MIN_MAX_PER_TEAM:
            case MIN_PER_TEAM:
                return getPlayerAmountBounds().getMin() * getNumberOfTeams();
            case NONE:
                return 1;
            default:
                return 2;
        }
    }

    public int getMaxPlayers() {
        switch (getPlayerAmountBounds().getType()) {
            case NONE:
            case MIN:
            case MIN_PER_TEAM:
                return Bukkit.getMaxPlayers();
            case MIN_MAX:
                return getPlayerAmountBounds().getMax();
            case MIN_MAX_PER_TEAM:
                return getPlayerAmountBounds().getMax() * getNumberOfTeams();
            default:
                return Bukkit.getMaxPlayers();
        }
    }

    public MinigameAPI(JavaPlugin minigame, PlayerAmountBounds playerAmountBounds, Class<A> arenaClass, Class<T> teamClass, Defaults defaults) {
        this(minigame, playerAmountBounds, defaults, arenaClass, teamClass, null);
    }

    @SuppressWarnings("unchecked")
    public MinigameAPI(JavaPlugin minigame, PlayerAmountBounds playerAmountBounds, Defaults defaults, Class<A> arenaClass, Class<T> teamClass, C minigameConfig) {
        instance = (MinigameAPI<Arena, Team, AnnotationConfiguration>) this;
        if (!minigame.getClass().isAnnotationPresent(Minigame.class)) {
            MinigameAPIPlugin.getInstance().getLogger().severe(minigame.getName() + " is not a Minigame! Please add the @Minigame annotation");
            MinigameAPIPlugin.getInstance().getLogger().severe("Shutting down now...");
            Bukkit.getScheduler().scheduleSyncDelayedTask(minigame, Bukkit::shutdown, 960L);
            return;
        } else {
            MinigameAPIPlugin.getInstance().getLogger().info("Initializing " + minigame.getName());
        }
        commandHandler = new CommandHandler(minigame);
        commandHandler.setPermissionHandler(BeastCubeAPI::handlePermission);
        commandHandler.registerCommands(minigame);
        commandHandler.registerCommands(new Commands());

        Minigame annotation = minigame.getClass().getAnnotation(Minigame.class);

        this.config = new Config<AnnotationConfiguration>(minigame, minigameConfig);
        this.minigameType = annotation.minigameType();
        this.scoreboardMode = annotation.scoreboardMode();
        this.arenaClass = arenaClass;
        this.teamClass = teamClass;
        this.playerAmountBounds = playerAmountBounds;
        this.defaults = defaults;
        this.minigame = minigame;
        state = MinigameState.WAITING_FOR_PLAYERS;
        Bukkit.getPluginManager().registerEvents(new MinigameEventListener(), minigame);
        Bukkit.getPluginManager().registerEvents(new TeamChatListener(), getMinigame());

        BeastCubeAPI.setNametagEnabled(false);
        BeastCubeAPI.setChatNameEnabled(false);
        BeastCubeSlave.getInstance().getServer().getMessenger().registerListener(new IncomingMessageHandler());
        loadLobby();
        loadMaps();
        MapChoosing.init();
        //TeamChoosing.init();

        final LoadingIndicator li = new LoadingIndicator();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(getMinigame(), () -> {
            if (getState() == MinigameState.WAITING_FOR_PLAYERS) {
                String indicator = li.next();
                Util.broadcastActionBarMessage(ChatColor.DARK_RED + indicator + ChatColor.GOLD + " Warte auf Spieler " + ChatColor.DARK_RED + indicator);
            }
        }, 0L, 5L);
        Messenger.sendMinigameState(false);
    }

    public void endGame(Object winner) {
        if (setState(MinigameState.RESTARTING)) {
            GameEndEvent event;
            if (winner instanceof Player) {
                event = new GameEndEvent(new TitleObject(ChatColor.GREEN + "Das Spiel ist beendet!",
                        ChatColor.LIGHT_PURPLE + "Gewinner: " + ChatColor.GOLD + ((Player) winner).getName()),
                        Collections.singletonList((Player) winner), null);
            } else if (winner != null && winner instanceof Team) {
                event = new GameEndEvent(new TitleObject(ChatColor.GREEN + "Das Spiel ist beendet!",
                        ChatColor.LIGHT_PURPLE + "Gewinner: " + ChatColor.GOLD + ((Team) winner).getName()),
                        ((Team) winner).getPlayers(), (Team) winner);
            } else {
                event = new GameEndEvent(new TitleObject(ChatColor.GREEN + "Das Spiel ist beendet!",
                        ChatColor.GRAY + "Unentschieden!"),
                        new ArrayList<>(), null);
            }
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                Util.broadcastTitleMessage(event.getMessage());
                if (Countdowns.getCountdown() != Countdowns.restartCountdown) {
                    Countdowns.stopCountdown();

                    Countdowns.setCountdown(Countdowns.restartCountdown);
                    Countdowns.getCountdown().start(getMinigame());
                }
            } else {
                if (Countdowns.getCountdown() == Countdowns.restartCountdown)
                    Countdowns.stopCountdown();
            }

            //Stats
            MinigameAPI.getInstance().getPlayersIngame().forEach(this::saveStats);
        }
    }

    public void saveStats(Player p) {
        if (this.getRoundStatsCache().getStats(p.getUniqueId()).isPresent()) {
            p.sendMessage(ChatColor.GOLD + "----------  Stats:  ----------");
            this.getRoundStatsCache().getStats(p.getUniqueId()).get().forEach((stat, value) -> p.sendMessage(stat.getDisplayName() + ": " + ChatColor.GOLD + stat.format(value)));

            if (this.getRoundStatsCache().getStat(p.getUniqueId(), BedwarsStats.BedwarsStat.POINTS).isPresent()) {
                Coins.addCoins(p.getUniqueId(), this.getRoundStatsCache().getStat(p.getUniqueId(), BedwarsStats.BedwarsStat.POINTS).get().intValue());
                p.sendMessage(ChatColor.GREEN + "Du hast " + this.getRoundStatsCache().getStat(p.getUniqueId(), BedwarsStats.BedwarsStat.POINTS) + " Coins erhalten.");
            }
        }
        this.getRoundStatsCache().saveStats(p.getUniqueId());
    }

    public void disable() {
        Messenger.sendMinigameState(true);
    }

    public void switchToDevelopment() {
        Countdowns.stopCountdown();
        setState(MinigameState.DEVELOPMENT);
        Bukkit.broadcastMessage("Das Spiel befindet sich nun im Entwicklungsmodus.");
    }

    public void saveDevelopmentChanges() { //TODO remove?
        setLobbySpawn(getLobbySpawn().get());
        config.save();
        if (getArena() != null) {
            try {
                File file = new File(Util.getWorldsFolder() + getArena().getName() + "/arena.json");
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));

                IO.saveWorld(getArena().getWorld().getName(), "");
                IO.copyMap(new File(Util.getWorldsFolder() + getArena().getName()), getArenaFolder(getArena().getName()));
            } catch (Exception e) {
                e.printStackTrace();
                Bukkit.broadcastMessage(ChatColor.RED + "Die Veränderungen konnten aufgrund eines Fehlers nicht gespeichert werden.");
            }
        }
        Bukkit.broadcastMessage(ChatColor.GREEN + "Die Veränderungen wurden erfolgreich gespeichert.");
    }

    public void loadRandomArena() {
        List<String> mostwanted = MapChoosing.getMostWanted();
        if (mostwanted.size() > 1) {
            Random random = new Random();
            setArena(mostwanted.get(random.nextInt(mostwanted.size())));
        } else if (mostwanted.size() == 1)
            setArena(mostwanted.get(0));
        else {
            Random random = new Random();
            List<A> maps = getMaps();
            setArena(maps.get(random.nextInt(maps.size())));
        }
    }

    //-----------------------------  Waiting Lobby  ----------------------------------

    public void loadLobby() {
        loadSettings();
        Bukkit.getOnlinePlayers().forEach(this::displayStats);
        displayRanking();
    }

    public void saveSettings() {
        if (getConfig().currentLobby != null) {
            try {
                File file = new File(getConfig().currentLobby.getWorldFolder() + "/lobby.json");
                FileUtils.writeStringToFile(file, Serializer.formatPretty(Serializer.serialize(lobbySettings)), "UTF-8");
            } catch (IOException e) {
                Providers.LOGGER.warning("Failed to save lobby.json!");
                e.printStackTrace();
            }
        } else {
            Providers.LOGGER.warning("Failed to save Lobby settings: currentHub not set");
        }
    }

    public void loadSettings() {
        if (getConfig().currentLobby != null) {
            File file = new File(getConfig().currentLobby.getWorldFolder() + "/lobby.json");
            try {
                if (file.exists()) {
                    String s = FileUtils.readFileToString(file, "UTF-8");
                    lobbySettings = Serializer.deserialize(s, LobbySettings.class);
                } else {
                    LobbySettings.class.newInstance();
                }
            } catch (IOException | InstantiationException | IllegalAccessException e) {
                Providers.LOGGER.warning("Failed to load lobby.json!");
                e.printStackTrace();
            }
        } else {
            Providers.LOGGER.warning("Failed to save Lobby settings: currentLobby not set");
        }
    }

    @SuppressWarnings("deprecation")
    public void displayRanking() {
        Location topleftcorner = lobbySettings.getRanking();
        Map<String, Long> top10 = minigameType.getStats().getTop10();
        PlayerDirection direction = lobbySettings.getRankingDirection();
        if (topleftcorner != null && direction != null) {
            if (direction.isOrthogonal() && !direction.isUpright()) {
                for (int i = 0; i < 10; i++) {
                    Location loc = topleftcorner.clone();
                    int offset = i;
                    if (i >= 5) {
                        offset -= 5;
                        loc.subtract(0, 2, 0);
                    }

                    BlockFace face = getBlockFaceFromPlayerDirection(direction);

                    loc.add(direction.vector().clone().multiply(offset));
                    Block b = loc.getBlock();
                    if (i < top10.size()) {
                        Map.Entry<String, Long> entry = new ArrayList<>(top10.entrySet()).get(i);
                        b.setType(Material.SKULL);
                        Skull skull = (Skull) b.getState();
                        skull.setOwner(entry.getKey());
                        MaterialData data = skull.getData();
                        data.setData(getRotatedRotationFromBlockFace(face));
                        skull.setData(data);
                        skull.update();

                        Location signLoc = loc.clone().subtract(0, 1, 0);
                        b = signLoc.getBlock();
                        b.setType(Material.WALL_SIGN, false);
                        Sign sign = (Sign) b.getState();
                        sign.setLine(0, ChatColor.DARK_RED + "" + (i + 1) + ". Platz");
                        sign.setLine(2, ChatColor.DARK_AQUA + entry.getKey());
                        sign.setLine(3, ChatColor.DARK_RED + "" + entry.getValue() + " Punkte");
                        MaterialData data2 = sign.getData();
                        data2.setData(getRotatedRotationFromBlockFace(face));
                        sign.setData(data2);
                        sign.update();
                    } else {
                        b.setType(Material.AIR);
                        Location signLoc = loc.clone().subtract(0, 1, 0);
                        signLoc.getBlock().setType(Material.AIR);
                    }
                }
            } else {
                Providers.LOGGER.warning("Invalid rankingDirection: " + direction.name());
            }
        } else {
            Providers.LOGGER.info("Ranking not set. Skipping");
        }
    }

    private byte getRotatedRotationFromBlockFace(BlockFace face) {
        switch (face) {
            case NORTH:
                return 0x2;
            case SOUTH:
                return 0x3;
            case WEST:
                return 0x4;
            case EAST:
                return 0x5;
            default:
                return 0x2;
        }
    }

    private BlockFace getBlockFaceFromPlayerDirection(PlayerDirection dir) {
        switch (dir) {
            case NORTH:
                return BlockFace.EAST;
            case EAST:
                return BlockFace.SOUTH;
            case SOUTH:
                return BlockFace.WEST;
            case WEST:
                return BlockFace.NORTH;
            default:
                return null;
        }
    }

    public void displayStats(Player p) {
        Location loc = lobbySettings.getStats();
        removeStats(p);
        if (loc != null) {
            statsHolograms.put(p.getUniqueId(), new ArrayList<>());
            Hologram statsHologram = HologramsAPI.createHologram(minigame, loc);
            statsHolograms.get(p.getUniqueId()).add(statsHologram);
            statsHologram.getVisibilityManager().showTo(p);
            statsHologram.getVisibilityManager().setVisibleByDefault(false);
            statsHologram.appendTextLine(ChatColor.GOLD + minigameType.getDisplayName());

            Optional<Map<Stat, Long>> stats = minigameType.getStats().getStats(p.getUniqueId());
            if (stats.isPresent()) {
                stats.get().keySet().forEach(stat -> statsHologram.appendTextLine(stat.getDisplayName() + ": " + ChatColor.GOLD + stat.format(stats.get().get(stat))));
            } else {
                statsHologram.appendTextLine("Du hast noch keine Runde " + minigameType.getDisplayName() + " gespielt.");
                statsHologram.appendTextLine("Spiele eine Runde um deine Statistik freizuschalten.");
            }
            statsHologram.teleport(statsHologram.getLocation().add(new org.bukkit.util.Vector(0, statsHologram.getHeight(), 0)));
        }
    }

    public void removeStats(Player p) {
        if (statsHolograms.containsKey(p.getUniqueId())) {
            statsHolograms.get(p.getUniqueId()).forEach(Hologram::delete);
            statsHolograms.remove(p.getUniqueId());
        }
    }


}
