package net.beastcube.minigameapi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.beastcube.api.bukkit.util.PlayerDirection;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import org.bukkit.Location;

/**
 * @author BeastCube
 */
@NoArgsConstructor
public class LobbySettings {

    @Getter
    @Setter
    @Serialize
    private Location spawn = null;
    @Getter
    @Setter
    @Serialize
    private Location stats = null;
    @Getter
    @Setter
    @Serialize
    private Location ranking = null;
    @Getter
    @Setter
    @Serialize
    private PlayerDirection rankingDirection;

}
