package net.beastcube.minigameapi;

import com.google.common.base.Joiner;
import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.DirectionUtil;
import net.beastcube.api.bukkit.util.serialization.Serializer;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.countdowns.Countdowns;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 *         <p>
 *         Contains some main development commands
 */
public class Commands {

    @Command(identifiers = "list", description = "Zeigt eine Liste aller Spieler und Zuschauer", onlyPlayers = false)
    public void showList(CommandSender sender) {
        List<Player> spectators = MinigameAPI.getInstance().getSpectators();
        List<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
        players.removeAll(spectators);

        if (players.size() > 0 || spectators.size() > 0) {
            if (players.size() > 0) {
                sender.sendMessage(ChatColor.GOLD + String.format("%s Spieler:", players.size()));
                sender.sendMessage(ChatColor.AQUA + Joiner.on(ChatColor.GRAY + ", " + ChatColor.AQUA).join(players.stream().map(Player::getDisplayName).collect(Collectors.toList())));
            }
            if (spectators.size() > 0) {
                sender.sendMessage(ChatColor.GOLD + String.format("%s Zuschauer:", spectators.size()));
                sender.sendMessage(ChatColor.AQUA + Joiner.on(ChatColor.GRAY + ", " + ChatColor.AQUA).join(spectators.stream().map(Player::getDisplayName).collect(Collectors.toList())));
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Keine Spieler online");
        }
    }

    @Command(identifiers = "setlobby", description = "Setzt die aktuelle Welt als Lobby", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void setLobby(Player sender) {
        MinigameAPI.getInstance().getConfig().currentLobby = sender.getLocation().getWorld();
        MinigameAPI.getInstance().getConfig().save();
        MinigameAPI.getInstance().loadLobby();
    }

    @Command(identifiers = "setlobbyspawn", description = "Setzt den Spawnpunkt für die Lobby", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void setLobbySpawn(Player sender) {
        MinigameAPI.getInstance().setLobbySpawn(sender.getLocation());
        MinigameAPI.getInstance().saveSettings();
        sender.sendMessage(ChatColor.GREEN + "Lobby Spawn erfolgreich gesetzt!");
    }

    @Command(identifiers = "setstats", description = "Setzt die Stats", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void setStats(Player sender) {
        MinigameAPI.getInstance().getLobbySettings().setStats(sender.getLocation());
        MinigameAPI.getInstance().saveSettings();
        Bukkit.getOnlinePlayers().forEach(player -> MinigameAPI.getInstance().displayStats(player));
        sender.sendMessage(ChatColor.GREEN + "Stats erfolgreich gesetzt!");
    }

    @Command(identifiers = "removestats", description = "Löscht die Stats", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void remoceStats(Player sender) {
        MinigameAPI.getInstance().getLobbySettings().setStats(null);
        MinigameAPI.getInstance().saveSettings();
        Bukkit.getOnlinePlayers().forEach(player -> MinigameAPI.getInstance().displayStats(player));
        sender.sendMessage(ChatColor.GREEN + "Stats erfolgreich gelöscht!");
    }

    @Command(identifiers = "setranking", description = "Setzt die linke obere Ecke der Rangliste. Schaue in die Richtung, in die die Rangliste expandiert werden soll", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void setRanking(Player sender) {
        MinigameAPI.getInstance().getLobbySettings().setRanking(sender.getLocation());
        MinigameAPI.getInstance().getLobbySettings().setRankingDirection(DirectionUtil.getCardinalDirection(sender));
        MinigameAPI.getInstance().saveSettings();
        Bukkit.getOnlinePlayers().forEach(player -> MinigameAPI.getInstance().displayRanking());
        sender.sendMessage(ChatColor.GREEN + "Rangliste erfolgreich gesetzt!");
    }

    @Command(identifiers = "removeranking", description = "Löscht die Rangliste", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void removeRanking(Player sender) {
        MinigameAPI.getInstance().getLobbySettings().setRanking(null);
        MinigameAPI.getInstance().getLobbySettings().setRankingDirection(null);
        MinigameAPI.getInstance().saveSettings();
        Bukkit.getOnlinePlayers().forEach(player -> MinigameAPI.getInstance().displayRanking());
        sender.sendMessage(ChatColor.GREEN + "Rangliste erfolgreich gelöscht!");
    }

    @Command(identifiers = "start", description = "Startet das Spiel", permissions = Permissions.MINIGAMEAPI_GAME_START)
    public void startGame(Player sender) {
        if (MinigameAPI.getInstance().getState() == MinigameState.LOBBY_COUNTDOWN_1) {
            Countdowns.getCountdown().setTime(0);
            sender.sendMessage(ChatColor.GREEN + "Das Spiel wird jetzt gestartet!");
        } else {
            sender.sendMessage(ChatColor.RED + "Das Spiel kann nur während der " + MinigameState.LOBBY_COUNTDOWN_1.getStateMessage() + " gestartet werden");
        }
    }

    //Used for StatusDisplay
    @Command(identifiers = "giveItem", description = "Gibt dir das serialisierte Item", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void giveItem(Player sender, @Arg(name = "item", description = "Serialisiertes item") String item) {
        sender.getInventory().addItem(Serializer.deserialize(item, ItemStack.class));
    }




    //TODO Remove commands below

    @Deprecated
    @Command(identifiers = "development", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void development(Player sender) {
        MinigameAPI.getInstance().switchToDevelopment();
    }

    @Deprecated
    @Command(identifiers = "save", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void save(Player sender) {
        MinigameAPI.getInstance().saveDevelopmentChanges();
    }

    @Deprecated
    @Command(identifiers = "loadmap", description = "Ladet eine Map um diese im Entwicklungsmodus zu verändern", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void loadMap(Player sender, @Arg(name = "map") String map) {
        File path = MinigameAPI.getInstance().getArenaFolder(map);
        if (path.exists() && path.isDirectory()) {
            MinigameAPI.getInstance().setArena(map);
            sender.sendMessage(ChatColor.GREEN + "Die Map wurde erfolgreich geladen.");
            return;
        }
        sender.sendMessage(ChatColor.RED + "Diese Map existiert nicht!");
    }

    @Deprecated
    @Command(identifiers = "teleporttoarena", description = "Teleportiert den Spieler in die Arena", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void teleportToArena(Player sender) {
        if (MinigameAPI.getInstance().getArena() == null) {
            sender.sendMessage(ChatColor.RED + "Es wurde keine Map geladen! /loadmap [map]");
        } else {
            Arena a = MinigameAPI.getInstance().getArena();
            sender.teleport(a.getSpawn(sender) == null ?
                    (a.getSpectatorSpawn() == null ? a.getWorld().getSpawnLocation() :
                            a.getSpectatorSpawn()) : a.getSpawn(sender));
        }
    }

    @Deprecated
    @Command(identifiers = "teleporttolobby", description = "Teleportiert den Spieler in die Lobby", permissions = Permissions.MINIGAMEAPI_ADMIN)
    public void teleportToLobby(Player sender) {
        if (!MinigameAPI.getInstance().getLobbySpawn().isPresent()) {
            World world = Bukkit.getWorld("lobby");
            if (world != null) {
                sender.teleport(world.getSpawnLocation());
            } else {
                sender.sendMessage(ChatColor.RED + "Es existiert keine Lobby und es wurde kein Lobbyspawnpunkt gesetzt.");
            }
        } else {
            sender.teleport(MinigameAPI.getInstance().getLobbySpawn().get());
        }
    }
}
