package net.beastcube.minigameapi;

import net.beastcube.minigameapi.kits.Kit;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 *         <p>
 *         All the player specific data is put together in this class to be able to remove/add/edit all together
 */
public class PlayerData {
    private final int REMEMBER_LAST_DAMAGER = 120;

    private PlayerState state;
    private int score;
    private Kit kit;

    private class LastDamagerCounter {
        Player player;
        long time;

        LastDamagerCounter(Player player, long time) {
            set(player, time);
        }

        void set(Player player, long time) {
            this.player = player;
            this.time = time;
        }
    }

    private LastDamagerCounter lastDamagerCounter;

    public PlayerState getState() {
        return state;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public Player getLastDamager() {
        return lastDamagerCounter.player;
    }

    public void setLastDamager(Player player) {
        final World world;
        if (player != null) {
            world = player.getWorld();
        } else {
            world = Bukkit.getWorlds().get(0);
        }
        if (lastDamagerCounter == null)
            lastDamagerCounter = new LastDamagerCounter(player, world.getFullTime());
        else {
            lastDamagerCounter.player = player;
            lastDamagerCounter.time = world.getFullTime();
        }
        if (player != null) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(MinigameAPI.getInstance().getMinigame(), new Runnable() {
                @Override
                public void run() {
                    if (world.getFullTime() >= lastDamagerCounter.time + REMEMBER_LAST_DAMAGER) {
                        lastDamagerCounter.player = null;
                    }
                }
            }, REMEMBER_LAST_DAMAGER);
        }
    }

    public PlayerData(PlayerState state) {
        this.state = state;
        this.score = 0;
        this.kit = null;
        setLastDamager(null);
    }
}
