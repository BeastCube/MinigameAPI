package net.beastcube.minigameapi.network;

import net.beastcube.api.network.MessageHandler;
import net.beastcube.api.network.message.SlaveConnectedMessage;

/**
 * @author BeastCube
 */
public class IncomingMessageHandler {

    @MessageHandler
    public void onSlaveConnectedMessage(SlaveConnectedMessage message) {
        Messenger.sendMinigameState(false);
    }

}