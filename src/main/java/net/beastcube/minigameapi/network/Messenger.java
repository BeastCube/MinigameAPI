package net.beastcube.minigameapi.network;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlave;
import net.beastcube.api.commons.minigames.MinigameUpdateMessage;
import net.beastcube.minigameapi.MinigameAPI;

/**
 * @author BeastCube
 */
public final class Messenger {

    public static void sendMinigameState(boolean delete) {
        MinigameAPI mgapi = MinigameAPI.getInstance();
        BeastCubeSlave.getInstance().getServer().getMasterServerInfo().sendMessage(new MinigameUpdateMessage(BeastCubeAPI.getServerName(),
                mgapi.getMinigameType(),
                mgapi.getOnlinePlayers().size(),
                mgapi.getMaxPlayers(),
                mgapi.getArena() == null ? "" : mgapi.getArena().getName(),
                mgapi.getState(),
                mgapi.getDefaults().isJoinIngame(),
                delete));
        mgapi.getMinigame().getLogger().info("Updating minigame state");
    }

}
