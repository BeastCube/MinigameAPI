package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.api.bukkit.util.serialization.Serializer;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.util.SmoothSerializer;
import net.minecraft.server.v1_8_R3.Item;
import net.minecraft.server.v1_8_R3.MinecraftKey;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author BeastCube
 */
public class ItemStackStatusAdapter extends StatusAdapter<ItemStack> {

    public ItemStackStatusAdapter(Class<ItemStack> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(ItemStack itemStack, Status status, List<FancyMessage> messages, Player target) {
        FancyMessage message = messages.get(messages.size() - 1);
        message.then(SmoothSerializer.serializeItemStack(itemStack)).suggest(status.command());
        message.then(ChatColor.GRAY + " [");
        message.then(ChatColor.GOLD + "GET").command("/giveItem " + Serializer.serialize(itemStack, false));
        message.then(ChatColor.GRAY + "]");
        return messages;
    }

    private static String getMinecraftId(Material material) {
        for (MinecraftKey id : Item.REGISTRY.keySet()) {
            if (CraftItemStack.asNewCraftStack(Item.REGISTRY.get(id)).getType() == material)
                return id.a();
        }
        return "";
    }

}
