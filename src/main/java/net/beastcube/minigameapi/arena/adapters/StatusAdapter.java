package net.beastcube.minigameapi.arena.adapters;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import mkremins.fanciful.FancyMessage;
import net.beastcube.minigameapi.arena.Status;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public abstract class StatusAdapter<T> {

    @Getter
    private final Class<T> type;

    public abstract List<FancyMessage> serialize(T object, Status status, List<FancyMessage> messages, Player target);

    public boolean isApplicable(Object o) {
        return type.isAssignableFrom(o.getClass());
    }

}
