package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.minigameapi.arena.Status;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author BeastCube
 */
public class WorldStatusAdapter extends StatusAdapter<World> {

    private String indent = "    ";

    public WorldStatusAdapter(Class<World> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(World world, Status status, List<FancyMessage> messages, Player target) {
        FancyMessage message = messages.get(messages.size() - 1);
        message.then(world.getName());
        return messages;
    }

}
