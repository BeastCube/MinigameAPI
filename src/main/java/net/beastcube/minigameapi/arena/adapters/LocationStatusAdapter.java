package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.util.SmoothSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author BeastCube
 */
public class LocationStatusAdapter extends StatusAdapter<Location> {

    public LocationStatusAdapter(Class<Location> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(Location location, Status status, List<FancyMessage> messages, Player target) {
        FancyMessage message = messages.get(messages.size() - 1);
        message.then(SmoothSerializer.serializeLocation(location, 1, 0)).suggest(status.command());
        message.then(ChatColor.GRAY + " [");
        message.then(ChatColor.GOLD + "TP").command("/tp " + location.getX() + " " + location.getY() + " " + location.getZ() + " " + location.getYaw() + " " + location.getPitch());
        message.then(ChatColor.GRAY + "]");
        return messages;
    }

}
