package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.arena.StatusDisplay;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author BeastCube
 */
public class MapStatusAdapter extends StatusAdapter<Map> {

    private String indent = "    ";

    public MapStatusAdapter(Class<Map> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(Map map, Status status, List<FancyMessage> messages, Player target) {
        Set entries = map.entrySet();
        Iterator iterator = entries.iterator();

        while (iterator.hasNext()) {
            messages.add(new FancyMessage(indent));
            Map.Entry entry = (Map.Entry) iterator.next();
            StatusDisplay.applyToFancyMessage(entry.getKey(), status, messages, target);
            messages.get(messages.size() - 1).then(" => ").color(ChatColor.YELLOW);
            StatusDisplay.applyToFancyMessage(entry.getValue(), status, messages, target);
            if (iterator.hasNext()) {
                messages.get(messages.size() - 1).then(ChatColor.GRAY + ",");
            }
        }
        return messages;
    }

}
