package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.api.commons.CuboidRegion;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.arena.StatusDisplay;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author BeastCube
 */
public class CuboidRegionStatusAdapter extends StatusAdapter<CuboidRegion> {

    private String indent = "    ";

    public CuboidRegionStatusAdapter(Class<CuboidRegion> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(CuboidRegion cuboidRegion, Status status, List<FancyMessage> messages, Player target) {
        messages.addAll(StatusDisplay.applyToFancyMessage(cuboidRegion.getWorld(), status, StatusDisplay.createList(new FancyMessage(indent).then(ChatColor.GRAY + "World: ")), target));
        messages.addAll(StatusDisplay.applyToFancyMessage(cuboidRegion.getV1Location(), status, StatusDisplay.createList(new FancyMessage(indent).then(ChatColor.GRAY + "Pos1: ")), target));
        messages.addAll(StatusDisplay.applyToFancyMessage(cuboidRegion.getV2Location(), status, StatusDisplay.createList(new FancyMessage(indent).then(ChatColor.GRAY + "Pos2: ")), target));
        return messages;
    }

}
