package net.beastcube.minigameapi.arena.adapters;

import mkremins.fanciful.FancyMessage;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.arena.StatusDisplay;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;

/**
 * @author BeastCube
 */
public class ListStatusAdapter extends StatusAdapter<List> { //TODO Fix, not showing items

    private String indent = "    ";

    public ListStatusAdapter(Class<List> type) {
        super(type);
    }

    @Override
    public List<FancyMessage> serialize(List list, Status status, List<FancyMessage> messages, Player target) {
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            messages.add(new FancyMessage(indent));
            Object o = iterator.next();
            StatusDisplay.applyToFancyMessage(o, status, messages, target);
            if (iterator.hasNext()) {
                messages.get(messages.size() - 1).then(ChatColor.GRAY + ",");
            }
        }
        return messages;
    }

}
