package net.beastcube.minigameapi.arena;

import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.commands.Wildcard;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author BeastCube
 */
public abstract class Arena {
    @Serialize
    @Status(name = "SpectatorSpawn", command = "/setSpectatorSpawn")
    private Location spectatorSpawn;
    @Serialize
    @Status(name = "Builders", command = "/setBuilders")
    private List<String> builders;
    @Serialize
    @Status(name = "Icon", command = "/setIcon")
    private ItemStack icon;
    @Serialize
    @Status(name = "ColorSet", command = "/setColorSet")
    private Integer colorSet;
    @Status(name = "Name")
    private String name;

    private World world;

    public Arena() {
    }

    public Arena(Location spectatorSpawn, List<String> builders, ItemStack icon, int colorSet, String name) {
        this.spectatorSpawn = spectatorSpawn;
        this.builders = builders;
        this.icon = icon;
        this.colorSet = colorSet;
        this.name = name;
    }

    public Location getSpectatorSpawn() {
        return spectatorSpawn;
    }

    public void setSpectatorSpawn(Location loc) {
        spectatorSpawn = loc;
    }

    @Command(identifiers = "setspectatorspawn", description = "Setzt den Spawnpunkt für Spectator an die aktuelle Posistion")
    public void setSpectatorSpawn(Player sender) {
        setSpectatorSpawn(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Der Spectator-Spawnpunkt wurde erfolgreich gesetzt");
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void setIcon(ItemStack stack) {
        icon = stack;
    }

    @Command(identifiers = "seticon", description = "Setzt das Item, welches für die Welt im Mapwähler angezeigt werden wird")
    public void setIcon(Player sender) {
        if (sender.getItemInHand() != null && sender.getItemInHand().getType() != Material.AIR) {
            setIcon(sender.getItemInHand());
            sender.sendMessage(ChatColor.GREEN + "Das Icon der Map wurde erfolgreich gesetzt.");
        } else
            sender.sendMessage(ChatColor.RED + "Du musst ein Item in der Hand halten");
    }

    public List<String> getBuilders() {
        return builders;
    }

    public String getSerializedBuilders() {
        String s = "";
        for (String as : builders)
            s += as + ", ";
        return s.substring(0, s.length() - 2);
    }

    public void setBuilders(List<String> builders) {
        this.builders = builders;
    }

    public void setBuilders(String[] builders) {
        this.builders = new ArrayList<String>();
        if (builders != null)
            for (String b : builders)
                this.builders.add(b);
    }

    @Command(identifiers = "setbuilders", description = "Setzt eine Liste von jenen Personen, welche an dieser Map mitgearbeitet haben")
    public void setBuilders(Player sender, @Wildcard @Arg(name = "builders") String builders) {
        setBuilders(builders.split(" "));
        sender.sendMessage("Die Architekten der Map wurden erfolgreich gesetzt.");
    }

    public void addBuilders(List<String> builders) {
        if (this.builders == null)
            this.builders = new ArrayList<String>();
        this.builders.addAll(builders);
    }

    public void addBuilders(String[] builders) {
        if (this.builders == null)
            this.builders = new ArrayList<String>();
        this.builders.addAll(Arrays.asList(builders));
    }

    @Command(identifiers = "addbuilders", description = "Fügt eine oder mehrere Personen, welche an dieser Map mitgearbeitete haben, hinzu")
    public void addBuilders(Player sender, @Wildcard @Arg(name = "builders") String builders) {
        addBuilders(builders.split(" "));
        sender.sendMessage(ChatColor.GREEN + "Die Architekten wurden erfolgreich der Map hinzugefügt.");
    }

    public int getColorSet() {
        return colorSet;
    }

    public void setColorSet(int colorSet) {
        this.colorSet = colorSet;
    }

    @Command(identifiers = "setColorSet", description = "Setzt das verwendete Farben-Set")
    public void setColorSet(Player sender, @Arg(name = "set") int colorSet) {
        setColorSet(colorSet);
        sender.sendMessage(ChatColor.GREEN + "Das Farbenset wurde erfolgreich festgelegt.");
    }

    @Command(identifiers = "arenastatus", description = "Zeigt den momentanen Status der Arena an")
    //TODO Permissions for all commands
    public void displayArenaStatus(Player sender) {
        Class clazz = this.getClass();
        sender.sendMessage(ChatColor.YELLOW + "Arenastatus:");
        while (clazz != null) {
            for (Field f : clazz.getDeclaredFields()) {
                if (f.isAnnotationPresent(Status.class)) {
                    f.setAccessible(true);
                    try {
                        StatusDisplay.sendStatus(f.get(this), f.getAnnotation(Status.class), sender);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

    public abstract Location getSpawn(Player p);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

}
