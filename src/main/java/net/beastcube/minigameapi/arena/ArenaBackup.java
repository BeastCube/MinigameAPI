package net.beastcube.minigameapi.arena;

/**
 * @author BeastCube
 */
/*@Serialize(serializedFields = {"teams", "spectatorSpawn", "builders", "icon", "name"})
public class ArenaBackup {
    private ArrayList<Team> teams = new ArrayList<Team>();
    private Location spectatorSpawn;
    private List<String> builders;
    private ItemStack icon;
    private String name;

    private World world;

    private ArenaBackup() {
        //This constructor is needed for the serializer to work
    }
    public ArenaBackup(ArrayList<Team> teams, Location spectatorSpawn, List<String> builders, ItemStack icon, String name) {
        this.teams = teams;
        this.spectatorSpawn = spectatorSpawn;
        this.builders = builders;
        this.icon = icon;
        this.name = name;
    }

    public Location getSpectatorSpawn() {
        return spectatorSpawn;
    }
    public void setSpectatorSpawn(Location loc) {
        spectatorSpawn = loc;
    }
    public void addTeam(Team team) {
        teams.add(team);
    }
    public void removeTeam(Team team) {
        teams.remove(team);
    }
    public void removeTeam(String team) {
        teams.remove(getTeam(team));
    }
    public ItemStack getIcon() {
        return icon;
    }
    public void setIcon(ItemStack stack) {
        icon = stack;
    }
    public List<String> getBuilders() {
        return builders;
    }
    public String getSerializedBuilders() {
        String s = "";
        for(String as : builders)
            s += as+", ";
        return s.substring(0, s.length()-2);
    }
    public void setBuilders(List<String> builders) {
        this.builders = builders;
    }
    public void setBuilders(String[] builders) {
        this.builders = new ArrayList<String>();
        if(builders != null)
            for(String b : builders)
                this.builders.add(b);
    }
    public Location getSpawn(Player p) {
        return MinigameAPI.getPlayerState(p) == PlayerState.INGAME ? getPlayersTeam(p).getSpawn() : spectatorSpawn;
    }
    public String getName() {
        return name;
    }

    public int getNumberOfTeams() {
        return teams.size();
    }
    public Team getTeam(String name) {
        for(Team t : teams) {
            if(t.getName().equals(name))
                return t;
        }
        return null;
    }
    public Team getTeam(int i) {
        return teams.get(i);
    }
    public Team getPlayersTeam(Player p) {
        for(Team t : teams) {
            if(t.containsPlayer(p))
                return t;
        }
        return null;
    }
    public void setPlayersTeam(Player p, Team t) {
        Team current = getPlayersTeam(p);
        if(current != null)
            current.removePlayer(p);
        if(t != null)
            t.addPlayer(p);
    }
    public ArrayList<Team> getTeamsPlaying() {
        ArrayList<Team> res = new ArrayList<Team>();
        for(Team t : teams) {
            if(t.getNumberOfPlayersWithState(PlayerState.INGAME) > 0)
                res.add(t);
        }
        return res;
    }
    public Team getSmallestTeam() {
        if(teams.size() == 0)
            return null;
        Team smallest = teams.get(0);
        for(Team t : teams) {
            if(t.getNumberOfPlayers() < smallest.getNumberOfPlayers()) {
                smallest = t;
            }
        }
        return smallest;
    }
    public List<Team> getTeams() {
        return teams;
    }

    public World getWorld() {
        return world;
    }
    public void setWorld(World world) {
        this.world = world;
    }

    //SERIALIZE
    public Arena(FileConfiguration config, String name) {
        this.name = name;
        Serializer.deserialize(config, this);
    }

    /*
    public Arena(FileConfiguration config, String name) {
         try {
            //---------------teams-----------------------------//
            List<String> steams = config.getStringList("teams");
            teams = new ArrayList<Team>();
            Team team;
            for(String t : steams) {
                team = Serializer.deserializeTeam(t);
                teams.add(team);
                MinigameAPI.addTeam(team);
            }

            //---------------spectatorSpawn--------------------//
            String loc = config.getString("spectatorSpawn");
            spectatorSpawn = Bukkit.getWorlds().get(0).getSpawnLocation();
            if(loc != null)
                spectatorSpawn = Serializer.deserializeLocation(loc);

            //---------------Cuboid----------------------------//
            Vector vector1 = config.getVector("Cuboid.Vector1");
            Vector vector2 = config.getVector("Cuboid.Vector2");
            if(vector1 == null || vector2 == null)
                bounds = new Cuboid(new Vector(0,0,0), new Vector(0,0,0));
            else
                bounds = new Cuboid(vector1, vector2);

            //---------------icon, builders-------------------//
            icon = config.getItemStack("icon");
            if(icon == null) {
                icon = new ItemStack(Material.BARRIER);
            }
            builders = config.getStringList("builders");
            if(builders == null)
                builders = new ArrayList<String>();

            this.name = name;

        } catch (Exception ex) {
            MinigameAPI.getMinigame().getLogger().info("An exception occured whilst loading arena from config: "+ex.getMessage());
            ex.printStackTrace();
        }
    }
    public void toConfig(FileConfiguration config, String file) {
        ArrayList<String> steams = new ArrayList<String>();
        for(Team t : teams) {
            steams.add(Serializer.serializeTeam(t));
        }
        config.set("teams", new ArrayList<String>());
        config.set("teams", steams);

        config.set("spectatorSpawn", Serializer.serializeLocation(spectatorSpawn));

        config.set("Cuboid.Vector1", bounds.V1);
        config.set("Cuboid.Vector2", bounds.V2);

        config.set("icon", icon);
        config.set("builders", builders);

        try {
            config.save(file);
        } catch (IOException e) {
            Bukkit.broadcastMessage(ChatColor.RED + "Die Arena konnte nicht gespeichert werden!");
        }
    }
}*/
