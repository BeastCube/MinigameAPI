package net.beastcube.minigameapi.arena;

import mkremins.fanciful.FancyMessage;
import net.beastcube.api.commons.CuboidRegion;
import net.beastcube.minigameapi.arena.adapters.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author BeastCube
 */
public final class StatusDisplay {

    private static List<StatusAdapter> statusAdapters = new ArrayList<>();

    static {
        registerStatusAdapter(new CuboidRegionStatusAdapter(CuboidRegion.class));
        registerStatusAdapter(new ItemStackStatusAdapter(ItemStack.class));
        registerStatusAdapter(new ListStatusAdapter(List.class));
        registerStatusAdapter(new LocationStatusAdapter(Location.class));
        registerStatusAdapter(new MapStatusAdapter(Map.class));
        registerStatusAdapter(new WorldStatusAdapter(World.class));
    }

    public static void registerStatusAdapter(StatusAdapter statusAdapter) {
        statusAdapters.add(statusAdapter);
    }

    public static void sendStatus(Object object, Status status, Player target) {
        FancyMessage message = new FancyMessage(ChatColor.GOLD + status.name() + ": ").suggest(status.command());
        List<FancyMessage> messages = applyToFancyMessage(object, status, createList(message), target);
        messages.forEach(msg -> msg.send(target));
    }

    public static <T> List<T> createList(T o) {
        ArrayList<T> list = new ArrayList<>();
        list.add(o);
        return list;
    }

    @SuppressWarnings("unchecked")
    public static List<FancyMessage> applyToFancyMessage(Object object, Status status, List<FancyMessage> messages, Player target) {
        if (object != null) {
            Optional<StatusAdapter> statusAdapter = statusAdapters.stream().filter(p -> p.isApplicable(object)).findFirst();
            if (statusAdapter.isPresent()) {
                return statusAdapter.get().serialize(object, status, messages, target);
            } else {
                messages.get(messages.size() - 1).then(object.toString()).suggest(status.command() + " " + object.toString());
                return messages;
            }
        } else {
            if (messages.size() > 0) {
                return createList(messages.get(messages.size() - 1).then("null"));
            } else {
                return createList(new FancyMessage("null"));
            }
        }
    }

}
