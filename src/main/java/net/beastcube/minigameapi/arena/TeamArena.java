package net.beastcube.minigameapi.arena;

import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

/**
 * @author BeastCube
 */
public class TeamArena extends Arena {
    @Serialize
    @Status(name = "TeamSpawns", command = "/setteamspawn")
    private HashMap<String, Location> spawns = new HashMap<>();

    public TeamArena() {
        super();
    }

    public TeamArena(HashMap<String, Location> spawns, Location spectatorSpawn, List<String> builders, ItemStack icon, int colorSet, String name) {
        super(spectatorSpawn, builders, icon, colorSet, name);
        this.spawns = spawns;
    }

    public void setSpawn(String team, Location spawn) {
        spawns.put(team, spawn);
    }

    @Command(identifiers = "setteamspawn", description = "Setzt den Spawn für ein Team and die aktuelle Position")
    public void setSpawn(Player sender, @Arg(name = "team", description = "Das Team für welches der Spawnpunkt gesetzt werden soll") String team) {
        setSpawn(team, sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Der Spawn für das Team " + team + " wurde erfolgreich hinzugefügt.");
    }

    public void removeSpawn(String team) {
        spawns.remove(team);
    }

    @Command(identifiers = "removeteamspawn", description = "Entfernt den Spawnpunkt eines Teams")
    public void removeSpawn(Player sender, @Arg(name = "team", description = "Das team für welches der Spawnpunkt entfernt werden soll") String team) {
        spawns.remove(team);
        sender.sendMessage(ChatColor.GREEN + "Der Spawn für das Team " + team + " wurde erfolgreich entfernt.");
    }

    @Override
    public Location getSpawn(Player p) {
        if (MinigameAPI.getInstance().getPlayerState(p) == PlayerState.INGAME || MinigameAPI.getInstance().getPlayerState(p) == PlayerState.IN_LOBBY && MinigameAPI.getInstance().getPlayersTeam(p) != null) {
            if (MinigameAPI.getInstance().getPlayersTeam(p) != null) {
                String team = MinigameAPI.getInstance().getPlayersTeam(p).getName(); //TODO
                return spawns.get(team);
            } else {
                return getWorld().getSpawnLocation();
            }
        } else if (getSpectatorSpawn() != null) {
            return getSpectatorSpawn();
        } else {
            return getWorld().getSpawnLocation();
        }
    }

}
