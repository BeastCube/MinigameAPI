package net.beastcube.minigameapi.arena;

import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.util.SmoothSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author BeastCube
 */
public class RandomSpawnArena extends Arena {
    @Serialize
    @Status(name = "Spawns", command = "/addspawn")
    private List<Location> spawns;

    private List<Location> spawns() {
        if (spawns == null)
            spawns = new ArrayList<>();
        return spawns;
    }

    public RandomSpawnArena() {
        super();
    }

    public RandomSpawnArena(List<Location> spawns, Location spectatorSpawn, List<String> builders, ItemStack icon, int colorSet, String name) {
        super(spectatorSpawn, builders, icon, colorSet, name);
        this.spawns = spawns;
    }

    public void addSpawn(Location spawn) {
        spawns().add(spawn);
    }

    @Command(identifiers = "addspawn", description = "Fügt einen neuen Spawnpunkt an der aktuellen Position hinzu")
    public void addSpawn(Player sender) {
        addSpawn(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Es wurde erfolgreich ein neuer Spawn hinzugefügt.");
    }

    public void removeSpawn(Location spawn) {
        spawns().remove(spawn);
    }

    @Command(identifiers = "removespawn", description = "Entfernt den naheliegendsten Spawnpunkt. Maximale Reichweite: 3 Blöcke")
    public void removeSpawn(Player sender) {
        if (spawns().size() == 0)
            sender.sendMessage(ChatColor.RED + "Es existiert noch kein Spawn.");
        Location nearest = spawns().get(0);
        double smallestDistance = nearest.distanceSquared(sender.getLocation());
        double currentDistance;
        for (int i = 1; i < spawns().size(); i++) { //TODO Block iterator?
            currentDistance = spawns().get(i).distanceSquared(sender.getLocation());
            if (currentDistance < smallestDistance) {
                smallestDistance = currentDistance;
                nearest = spawns().get(i);
            }
        }
        if (smallestDistance > 3) { //TODO Shouldn't this be 9, because we use distanceSquared?
            sender.sendMessage(ChatColor.YELLOW + "Der naheliegendsten Spawnpunkt ist mehr als 3 Blöcke entfernt.");
            sender.sendMessage(ChatColor.YELLOW + "Er befindet sich bei: " + SmoothSerializer.serializeVector(nearest.toVector(), 2));
        } else {
            removeSpawn(nearest);
            sender.sendMessage(ChatColor.GREEN + "Der naheliegendsten Spawnpunkt wurde erfolgreich entfernt.");
        }
    }

    @Command(identifiers = "clearspawns", description = "Entfernt alle Spawnpunkte.")
    public void clearSpawns(Player sender) {
        spawns().clear();
    }

    @Override
    public Location getSpawn(Player p) {
        return (MinigameAPI.getInstance().getPlayerState(p) == PlayerState.INGAME || MinigameAPI.getInstance().getPlayerState(p) == PlayerState.IN_LOBBY) ? spawns().get(new Random().nextInt(spawns().size())) : getSpectatorSpawn();
    }

}
