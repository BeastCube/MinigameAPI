package net.beastcube.minigameapi;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author BeastCube
 *         <p>
 *         This class is only needed to make the api look like a spigot plugin so that
 *         spigot dependencies work
 */
public class MinigameAPIPlugin extends JavaPlugin {
    @Getter
    private static JavaPlugin instance;

    @Override
    public void onEnable() {
        instance = this;
    }
}
