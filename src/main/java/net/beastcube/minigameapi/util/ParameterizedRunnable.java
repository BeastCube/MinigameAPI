package net.beastcube.minigameapi.util;

public interface ParameterizedRunnable {
    void run(Object parameter);
}
