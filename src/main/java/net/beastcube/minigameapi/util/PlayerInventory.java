package net.beastcube.minigameapi.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class PlayerInventory {
    private Inventory inventory;
    private ItemStack[] armorContents;

    public PlayerInventory(Inventory inventory, ItemStack[] armorContents) {
        this.inventory = inventory;
        this.armorContents = armorContents;
    }

    public PlayerInventory() {
        this.inventory = Bukkit.createInventory(null, 27);
        armorContents = new ItemStack[4];
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public ItemStack[] getArmorContents() {
        return armorContents;
    }

    public void setArmorContents(ItemStack[] armorContents) {
        this.armorContents = armorContents;
    }

    public void apply(Player p) {
        p.getInventory().setContents(getInventory().getContents());
        p.getInventory().setArmorContents(getArmorContents());
    }
}
