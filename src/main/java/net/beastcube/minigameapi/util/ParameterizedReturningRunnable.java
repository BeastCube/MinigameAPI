package net.beastcube.minigameapi.util;

/**
 * @author BeastCube
 */

public interface ParameterizedReturningRunnable {
    Object run(Object parameter);
}
