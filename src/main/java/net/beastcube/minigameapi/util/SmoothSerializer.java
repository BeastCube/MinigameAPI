package net.beastcube.minigameapi.util;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

/**
 * @author BeastCube
 */
public final class SmoothSerializer {
    public static String serializeVector(Vector v, int round) {
        double pot = Math.pow(10, round);
        return ChatColor.GRAY + "( " + ChatColor.RED + (Math.round(v.getX() * pot) / pot) +
                ChatColor.GRAY + " | " + ChatColor.GREEN + (Math.round(v.getY() * pot) / pot) +
                ChatColor.GRAY + " | " + ChatColor.BLUE + (Math.round(v.getZ() * pot) / pot) +
                ChatColor.GRAY + " )" + ChatColor.RESET;
    }

    public static String serializeLocation(Location l, int roundCoords, int roundRotation) {
        double potC = Math.pow(10, roundCoords);
        double potR = Math.pow(10, roundRotation);
        return ChatColor.GRAY + l.getWorld().getName() +
                ChatColor.GRAY + " - ( " + ChatColor.RED + (Math.round(l.getX() * potC) / potC) +
                ChatColor.GRAY + " | " + ChatColor.GREEN + (Math.round(l.getY() * potC) / potC) +
                ChatColor.GRAY + " | " + ChatColor.BLUE + (Math.round(l.getZ() * potC) / potC) +
                ChatColor.GRAY + " ) ∠ ( " + ChatColor.RED + (Math.round(l.getYaw() * potR) / potR) +
                ChatColor.GRAY + " | " + ChatColor.YELLOW + (Math.round(l.getPitch() * potR) / potR) +
                ChatColor.GRAY + " ) " + ChatColor.RESET;
    }

    public static String serializeItemMeta(ItemMeta m) {
        return "...";
        //TODO: serialize item meta in a way that looks very smooth and nice
    }

    public static String serializeItemStack(ItemStack i) {
        return ChatColor.YELLOW + String.valueOf(i.getAmount()) + ChatColor.GRAY + "x" + ChatColor.RED + i.getType().toString() +
                ChatColor.GRAY + (i.getDurability() == 0 ? "" : " (" + ChatColor.BLUE + i.getDurability() + ChatColor.GRAY + ") ") +
                (i.hasItemMeta() ? serializeItemMeta(i.getItemMeta()) : "");
    }

}
