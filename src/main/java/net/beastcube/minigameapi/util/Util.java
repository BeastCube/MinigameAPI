package net.beastcube.minigameapi.util;

import net.beastcube.api.bukkit.util.title.ActionBarAPI;
import net.beastcube.api.bukkit.util.title.TitleAPI;
import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public final class Util {
    public static void broadcastActionBarMessage(String message) {
        for (Player p : MinigameAPI.getInstance().getOnlinePlayers())
            ActionBarAPI.sendActionBar(p, message);
    }

    public static void broadcastTitleMessage(String message) {
        for (Player p : MinigameAPI.getInstance().getOnlinePlayers()) {
            TitleAPI.sendTitle(p, 1, 480, 60, message, "");
        }
    }

    public static void broadcastTitleMessage(TitleObject obj) {
        MinigameAPI.getInstance().getOnlinePlayers().forEach(obj::send);
    }

    public static String getWorldsFolder() {
        return Bukkit.getWorldContainer().getAbsolutePath().substring(0, Bukkit.getWorldContainer().getAbsolutePath().length() - 1);
        //return Bukkit.getWorlds().get(0).getWorldFolder().getAbsoluteFile().getParentFile().getAbsolutePath();
    }
}
