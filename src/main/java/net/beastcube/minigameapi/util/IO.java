package net.beastcube.minigameapi.util;

import net.beastcube.minigameapi.Minigame;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * @author BeastCube
 */
public class IO {

    /**
     * -- author: mkyong --
     */
    public static void copyMap(File src, File dest) throws IOException {
        List<String> ignored = Collections.singletonList("session.lock");
        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
                MinigameAPI.getInstance().getMinigame().getLogger().info("Directory copied from "
                        + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyMap(srcFile, destFile);
            }

        } else if (!ignored.contains(src.getName())) { //TODO not only filename, but path
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            MinigameAPI.getInstance().getMinigame().getLogger().info("File copied from " + src + " to " + dest);
        } else {
            MinigameAPI.getInstance().getMinigame().getLogger().info("Skipped file " + src);
        }
    }

    public static void deleteFolder(File folder) {
        if (folder.isDirectory()) {
            for (File f : folder.listFiles()) {
                deleteFolder(f);
            }
            try {
                Files.delete(Paths.get(folder.getAbsolutePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Files.delete(Paths.get(folder.getAbsolutePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static World loadWorld(String world, File file) {
        try {
            String s = Bukkit.getWorlds().get(0).getWorldFolder().getAbsoluteFile().getParentFile().getAbsolutePath();
            String path = s.endsWith(File.separator) ? s : s + File.separator;
            File dir = new File(path + world);
            MinigameAPI.getInstance().getMinigame().getLogger().info("Copying " + file.getAbsolutePath() + " to " + dir.getAbsolutePath());
            if (!file.exists()) {
                Bukkit.broadcastMessage(ChatColor.RED + "Die Welt konnte nicht geladen werden");
                return null;
            }
            copyMap(file, dir);
            MinigameAPI.getInstance().getMinigame().getLogger().info("Folder copied!");
            WorldCreator worldCreator = new WorldCreator(world);
            worldCreator.generator(MinigameAPI.getInstance().getMinigame().getClass().getAnnotation(Minigame.class).worldGenerator()); //TODO WorldGenerator in arena.yml
            return Bukkit.getServer().createWorld(worldCreator);
        } catch (Exception ex) {
            Bukkit.broadcastMessage(ChatColor.RED + "Die Welt " + world + " konnte nicht geladen werden!");
            ex.printStackTrace();
            return null;
        }
    }

    public static void saveWorld(String world, String path) {
        try {
            File dir = Bukkit.getWorld(world).getWorldFolder().getAbsoluteFile();
            File file = new File(path + "/" + world);
            MinigameAPI.getInstance().getMinigame().getLogger().info("Copying " + dir.getAbsolutePath() + " to " + file.getAbsolutePath());
            if (!dir.exists()) {
                Bukkit.broadcastMessage(ChatColor.RED + "Die Welt konnte nicht geladen werden");
                return;
            }
            copyMap(dir, file);
        } catch (Exception ex) {
            Bukkit.broadcastMessage(ChatColor.RED + "Die Welt " + world + " konnte nicht geladen werden!");
            ex.printStackTrace();
        }
    }

}
