package net.beastcube.minigameapi.defaults;

/**
 * @author BeastCube
 */
public class PlayerAmountBounds {
    PlayerAmountBoundType type;
    int min;
    int max;

    public PlayerAmountBounds(PlayerAmountBoundType type) {
        this.type = type;
    }

    public PlayerAmountBounds(PlayerAmountBoundType type, int min) {
        this.type = type;
        this.min = min;
    }

    public PlayerAmountBounds(PlayerAmountBoundType type, int min, int max) {
        this.type = type;
        this.min = min;
        this.max = max;
    }

    public PlayerAmountBounds setMin(int min) {
        this.min = min;
        return this;
    }

    public PlayerAmountBounds setMax(int max) {
        this.max = max;
        return this;
    }

    public PlayerAmountBoundType getType() {
        return type;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
