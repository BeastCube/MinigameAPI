package net.beastcube.minigameapi.defaults;

import lombok.Data;
import org.bukkit.GameMode;

/**
 * @author BeastCube
 */
@Data
public class Defaults {

    //The defaults. The MinigameAPI will implement some events like the BlockBreakEvent and cancel them if blockBreaking is set to false.
    //Using a low priority this can be overwritten.

    private boolean blockBreaking = true;
    private boolean blockPlacing = true;
    private boolean containerOpening = true;
    private boolean inventoryOpening = true;
    private boolean hunger = true;
    private boolean crafting = true;
    private boolean dieBeneathZero = true;
    private boolean autoRespawn = false;
    private boolean blockExploding = true;
    private boolean flintAndSteel = true;
    private boolean dropStuffOnDeath = true;
    private boolean displayIngameCountdown = true;
    private boolean daylightCycle = true;
    private boolean weatherChange = false;
    private boolean enterBedAllowed = true;
    private boolean destroySoil = true;
    private boolean hangingBreak = true;
    private boolean teamChat = true;
    private boolean blockBurning = true;
    private boolean tntDamage = true;
    private boolean dropItems = true;

    /**
     * playerDamage is more important than teammateDamage.
     */
    private boolean playerDamage = true;
    private boolean friendlyFire = false;
    private boolean keepInventoryOnDeath = false;
    private boolean joinIngame = false;

    private GameMode defaultGamemode = GameMode.SURVIVAL;

    /**
     * set this to -1 to make the game last forever
     */
    private int ingameCountdown = 7200;

}
