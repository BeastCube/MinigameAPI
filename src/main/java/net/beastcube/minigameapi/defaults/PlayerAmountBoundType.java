package net.beastcube.minigameapi.defaults;

/**
 * @author BeastCube
 */
public enum PlayerAmountBoundType {
    NONE,
    MIN_MAX,
    MIN,
    MIN_PER_TEAM,
    MIN_MAX_PER_TEAM
}