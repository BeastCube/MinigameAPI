package net.beastcube.minigameapi;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.config.annotation.Setting;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class Config<C extends AnnotationConfiguration> extends AnnotationConfiguration {
    public World currentLobby;
    @Setting
    public String worldsFolder = "";

    public final C minigameConfig;

    private Plugin plugin = null;
    private FileConfiguration config = null;

    public Config(Plugin plugin, C minigameConfig) {
        this.plugin = plugin;
        config = plugin.getConfig();
        this.minigameConfig = minigameConfig;
        this.load();
        this.save();
    }

    public void load() {
        load(config);
    }

    public void save() {
        save(config);
        plugin.saveConfig();
    }

    @Override
    public void load(ConfigurationSection section) {
        super.load(section);
        if (section.contains("currentLobby")) {
            currentLobby = Bukkit.getWorld(section.getString("currentLobby"));
        }
    }

    @Override
    public void save(ConfigurationSection section) {
        super.save(section);
        if (currentLobby != null) {
            section.set("currentLobby", currentLobby.getName());
        }
    }
}
