package net.beastcube.minigameapi.events;

import net.beastcube.api.bukkit.util.title.TitleObject;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class GameStartEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private TitleObject message;

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public TitleObject getMessage() {
        return message;
    }

    public void setMessage(TitleObject message) {
        this.message = message;
    }

    public GameStartEvent(TitleObject message) {
        this.message = message;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
