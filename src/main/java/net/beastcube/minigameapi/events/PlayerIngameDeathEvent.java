package net.beastcube.minigameapi.events;

import net.beastcube.minigameapi.util.PlayerInventory;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author BeastCube
 */
public final class PlayerIngameDeathEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private PlayerDeathEvent coreEvent;
    private PlayerInventory inventory;

    public PlayerIngameDeathEvent(PlayerDeathEvent coreEvent) {
        this.coreEvent = coreEvent;
        inventory = new PlayerInventory();
    }

    public PlayerIngameDeathEvent(PlayerDeathEvent coreEvent, PlayerInventory inventory) {
        this.coreEvent = coreEvent;
        this.inventory = inventory;
    }

    public Player getPlayer() {
        return coreEvent.getEntity();
    }

    public String getDeathMessage() {
        return coreEvent.getDeathMessage();
    }

    public void setDeathMessage(String deathMessage) {
        coreEvent.setDeathMessage(deathMessage);
    }

    public boolean getKeepInventory() {
        return coreEvent.getKeepInventory();
    }

    public void setKeepInventory(boolean keepInventory) {
        coreEvent.setKeepInventory(keepInventory);
    }

    public int getNewExp() {
        return coreEvent.getNewExp();
    }

    public void setNewExp(int exp) {
        coreEvent.setNewExp(exp);
    }

    public int getNewLevel() {
        return coreEvent.getNewLevel();
    }

    public void setNewLevel(int level) {
        coreEvent.setNewLevel(level);
    }

    public int getNewTotalExp() {
        return coreEvent.getNewTotalExp();
    }

    public void setNewTotalExp(int totalExp) {
        coreEvent.setNewTotalExp(totalExp);
    }

    public boolean getKeepLevel() {
        return coreEvent.getKeepLevel();
    }

    public void setKeepLevel(boolean keepLevel) {
        coreEvent.setKeepLevel(keepLevel);
    }

    public int getDroppedExp() {
        return coreEvent.getDroppedExp();
    }

    public void setDroppedExp(int exp) {
        coreEvent.setDroppedExp(exp);
    }

    public List<ItemStack> getDrops() {
        return coreEvent.getDrops();
    }

    public PlayerInventory getInventory() {
        return inventory;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
