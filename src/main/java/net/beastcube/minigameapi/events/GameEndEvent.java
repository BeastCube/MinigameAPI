package net.beastcube.minigameapi.events;

import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

/**
 * @author BeastCube
 */
public final class GameEndEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Team winnerTeam;
    private TitleObject message;
    private List<Player> winners;

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public void setWinnerTeam(Team winnerTeam) {
        this.winnerTeam = winnerTeam;
    }

    public Team getWinnerTeam() {
        return winnerTeam;
    }

    public void setWinners(List<Player> winners) {
        this.winners = winners;
    }

    public List<Player> getWinners() {
        return this.winners;
    }

    public TitleObject getMessage() {
        return message;
    }

    public void setMessage(TitleObject message) {
        this.message = message;
    }

    public GameEndEvent(TitleObject message, List<Player> winners, Team winnerTeam) {
        this.message = message;
        this.winners = winners;
        this.winnerTeam = winnerTeam;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
