package net.beastcube.minigameapi.events;

import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class TeamStateChangedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private PlayerState playerState;
    private Team team;
    private String broadcastMessage;
    private String message;
    private TitleObject broadcastTitleMessage;
    private TitleObject titleMessage;

    public TeamStateChangedEvent(PlayerState playerState, Team team) {
        this.playerState = playerState;
        this.team = team;
    }

    public PlayerState getState() {
        return playerState;
    }

    public Team getTeam() {
        return team;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(String broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }

    public TitleObject getTitleMessage() {
        return titleMessage;
    }

    public TitleObject getBroadcastTitleMessage() {
        return broadcastTitleMessage;
    }

    public void setTitleMessage(TitleObject titleMessage) {
        this.titleMessage = titleMessage;
    }

    public void setBroadcastTitleMessage(TitleObject broadcastTitleMessage) {
        this.broadcastTitleMessage = broadcastTitleMessage;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
