package net.beastcube.minigameapi.events;

import net.beastcube.minigameapi.PlayerData;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author BeastCube
 */
public final class PlayerIngameQuitEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private PlayerQuitEvent coreEvent;
    private PlayerData playerData;
    private Team playersTeam;

    public PlayerIngameQuitEvent(PlayerQuitEvent coreEvent, PlayerData playerData, Team playersTeam) {
        this.coreEvent = coreEvent;
        this.playerData = playerData;
        this.playersTeam = playersTeam;
    }

    public Player getPlayer() {
        return coreEvent.getPlayer();
    }

    public String getQuitMessage() {
        return coreEvent.getQuitMessage();
    }

    public void setQuitMessage(String quitMessage) {
        coreEvent.setQuitMessage(quitMessage);
    }

    public PlayerData getPlayerData() {
        return playerData;
    }

    public Team getPlayersTeam() {
        return playersTeam;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
