package net.beastcube.minigameapi.events;

import net.beastcube.api.commons.minigames.MinigameState;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class GameStateChangedEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private MinigameState gameState;
    private boolean cancelled;
    private String message;

    public GameStateChangedEvent(MinigameState gameState) {
        this.gameState = gameState;
    }

    public MinigameState getGameState() {
        return gameState;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
