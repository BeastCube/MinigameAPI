package net.beastcube.minigameapi.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public final class PlayerIngameJoinEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private PlayerJoinEvent coreEvent;
    private Inventory inventory;
    private ItemStack[] inventoryArmorContents;

    public PlayerIngameJoinEvent(PlayerJoinEvent coreEvent) {
        this.coreEvent = coreEvent;
        this.inventoryArmorContents = new ItemStack[4];
        inventory = Bukkit.createInventory(null, 27);
    }

    public Player getPlayer() {
        return coreEvent.getPlayer();
    }

    public String getJoinMessage() {
        return coreEvent.getJoinMessage();
    }

    public void setJoinMessage(String joinMessage) {
        coreEvent.setJoinMessage(joinMessage);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public ItemStack[] getInventoryArmorContents() {
        return inventoryArmorContents;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
