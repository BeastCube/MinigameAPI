package net.beastcube.minigameapi.events;

import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class TeamPlayerCountChangedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private int playerCount;
    private Team team;
    private String messageToTeam;
    private String broadcastMessage;
    private TitleObject titleMessage;
    private TitleObject broadcastTitleMessage;

    public TeamPlayerCountChangedEvent(Team team, int playerCount) {
        this.playerCount = playerCount;
        this.team = team;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public Team getTeam() {
        return team;
    }

    public String getMessageToTeam() {
        return messageToTeam;
    }

    public void setMessageToTeam(String messageToTeam) {
        this.messageToTeam = messageToTeam;
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(String broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }

    public TitleObject getTitleMessage() {
        return titleMessage;
    }

    public TitleObject getBroadcastTitleMessage() {
        return broadcastTitleMessage;
    }

    public void setTitleMessage(TitleObject titleMessage) {
        this.titleMessage = titleMessage;
    }

    public void setBroadcastTitleMessage(TitleObject broadcastTitleMessage) {
        this.broadcastTitleMessage = broadcastTitleMessage;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
