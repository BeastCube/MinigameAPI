package net.beastcube.minigameapi.events;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.minigameapi.PlayerState;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class PlayerStateChangedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private PlayerState playerState;
    private Player player;
    private String broadcastMessage;
    private String message;

    public PlayerStateChangedEvent(PlayerState playerState, Player player) {
        this.playerState = playerState;
        this.player = player;

        switch (playerState) {
            case SPECTATOR:
                broadcastMessage = ChatColor.GOLD + player.getName() + " schaut jetzt zu.";
                message = ChatColor.GOLD + "Du schaust jetzt " + ChatColor.AQUA + BeastCubeAPI.getServerName() + ChatColor.GOLD + " zu.";
                break;
            case LOST: //TODO Called before the last player in bedwars died in chat
                broadcastMessage = null;
                message = ChatColor.RED + "----------------------------------------\\n" + ChatColor.RED + "Du hast verloren!\\n" + ChatColor.RED + "----------------------------------------";
                break;
            case WON:
                broadcastMessage = ChatColor.GREEN + player.getName() + " hat gewonnen.";
                message = ChatColor.RED + "----------------------------------------\\n" + ChatColor.RED + "Du hast gewonnen!\\n" + ChatColor.RED + "----------------------------------------";
                break;
            default:
                broadcastMessage = null;
                break;
        }
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public Player getPlayer() {
        return player;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(String broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }


    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
