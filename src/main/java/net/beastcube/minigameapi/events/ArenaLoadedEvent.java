package net.beastcube.minigameapi.events;

import net.beastcube.minigameapi.arena.Arena;
import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
public final class ArenaLoadedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Arena arena;
    private World world;

    public Arena getArena() {
        return arena;
    }

    public World getWorld() {
        return world;
    }

    public ArenaLoadedEvent(Arena arena, World world) {
        this.arena = arena;
        this.world = world;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
