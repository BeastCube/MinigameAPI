package net.beastcube.minigameapi;

/**
 * @author BeastCube
 */
public enum PlayerState {
    NONE(false),
    IN_LOBBY(false),
    INGAME(false),
    LOST(true),
    WON(true),
    SPECTATOR(true);

    private boolean outOfGame;

    PlayerState(boolean outOfGame) {
        this.outOfGame = outOfGame;
    }

    public boolean isOutOfGame() {
        return outOfGame;
    }
}
