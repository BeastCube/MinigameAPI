package net.beastcube.minigameapi.kits;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class KitMenuItem extends MenuItem {
    ItemStackKit kit;

    private static ItemStack getKitItemStack(ItemStackKit kit, Player p) {
        ItemStack stack = kit.createItemStack();
        if (MinigameAPI.getInstance().getPlayerKit(p) == kit)
            stack.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
        return new ItemBuilder(stack).addAllFlags().build();
    }

    public KitMenuItem(ItemStackKit kit, Player p) {
        super(ChatColor.DARK_PURPLE + kit.getName(), getKitItemStack(kit, p), ChatColor.GOLD + kit.getKitDescription());
        this.kit = kit;
    }


    @Override
    public void onItemClick(ItemClickEvent e) {
        MinigameAPI.getInstance().setPlayerKit(e.getPlayer(), kit);
        e.setAction(ItemClickEvent.ItemClickAction.CLOSE);
        e.getPlayer().sendMessage(ChatColor.GREEN + "Du hast das Kit " + kit.getName() + " gewählt!");
    }

}
