package net.beastcube.minigameapi.kits;

import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
public final class KitChoosing {

    private static ArrayList<Kit> Kits = new ArrayList<Kit>();
    private static KitChooserInteractItem kitChooser;

    public static void setKitChooser(KitChooserInteractItem kitChooser) {
        if (kitChooser != null) {
            KitChoosing.kitChooser = kitChooser;
        }
    }

    public static void registerKit(ItemStackKit kit) {
        Kits.add(kit);
    }

    public static ItemStackKit getKit(ItemStack stack) {
        if (stack != null) {
            for (Kit k : Kits) {
                if (k instanceof ItemStackKit) {
                    if (((ItemStackKit) k).isKit(stack)) {
                        return (ItemStackKit) k;
                    }
                }
            }
        }
        return null;
    }

    public static ItemStackKit getKit(int i) {
        Kit kit = Kits.get(i);
        if (kit instanceof ItemStackKit)
            return (ItemStackKit) kit;
        else return null;
    }

    public static int getKitsLength() {
        return Kits.size();
    }

    public static ArrayList<Kit> getKits() {
        return (ArrayList<Kit>) Kits.clone();
    }

    public static void giveToPlayer(Player p) {
        if (kitChooser != null)
            kitChooser.giveToPlayer(p);
    }

    public static void takeFromPlayer(Player p) {
        if (kitChooser != null)
            kitChooser.takeFromPlayer(p);
    }

    public static void applyKits() {
        MinigameAPI.getInstance().getOnlinePlayers().forEach(KitChoosing::applyKit);
    }

    @SuppressWarnings("deprecation")
    public static void applyKit(Player p) {
        Kit k = MinigameAPI.getInstance().getPlayerKit(p);
        if (k != null) {
            k.FillPlayersInventory(p);
            k.ApplyPotionEffects(p);
        } else {
            p.getInventory().clear();
            while (p.getActivePotionEffects().size() > 0) {
                p.removePotionEffect(p.getActivePotionEffects().iterator().next().getType());
            }
        }
        p.updateInventory();
    }

}
