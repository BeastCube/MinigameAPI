package net.beastcube.minigameapi.kits;

import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public interface KitChooser {
    void giveToPlayer(Player p);

    void takeFromPlayer(Player p);
}
