package net.beastcube.minigameapi.kits;

import net.beastcube.minigameapi.util.PlayerInventory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import javax.annotation.Nullable;

/**
 * @author BeastCube
 */
public class Kit {

    private int ID;
    private String Name;
    private String Desciption;
    private org.bukkit.inventory.Inventory Inventory;
    private ItemStack[] Armor;
    private PotionEffect[] PotionEffects;

    public Kit(String name, String description, @Nullable org.bukkit.inventory.Inventory inventory, @Nullable ItemStack[] armor, @Nullable PotionEffect[] potioneffects, int id) {
        Name = name;
        Desciption = description;
        Inventory = inventory == null ? Bukkit.createInventory(null, InventoryType.PLAYER) : inventory;
        ItemStack air = new ItemStack(Material.AIR);
        Armor = isArmorValid(armor) ? armor : new ItemStack[]{air, air, air, air};
        if (potioneffects == null)
            PotionEffects = new PotionEffect[0];
        else {
            PotionEffects = new PotionEffect[potioneffects.length];
            for (int i = 0; i < potioneffects.length; i++)
                PotionEffects[i] = new PotionEffect(potioneffects[i].getType(), Integer.MAX_VALUE, potioneffects[i].getAmplifier());
        }
        ID = id;
    }

    public PlayerInventory getPlayerInventory() {
        return new PlayerInventory(Inventory, Armor);
    }

    public String getKitDescription() {
        return Desciption;
    }

    public String getName() {
        return Name;
    }

    public ItemStack getItemInSlot(int i) {
        if (i > 32 || i < 0)
            throw new ArrayIndexOutOfBoundsException("i has to be bigger than 0 and smaller than 32!");
        return Inventory.getItem(i);
    }

    public ItemStack getItemInArmorSlot(int i) {
        if (i > 3 || i < 0)
            throw new ArrayIndexOutOfBoundsException("i has to be bigger than 0 and smaller than 4!");
        return Armor[i];
    }

    public PotionEffect[] getPotionEffects() {
        return PotionEffects;
    }

    public int getID() {
        return ID;
    }

    public void FillPlayersInventory(final Player p) {
        p.getInventory().clear();
        p.getInventory().setContents(Inventory.getContents());
        p.getInventory().setArmorContents(Armor);
    }

    public void ApplyPotionEffects(Player p) {
        for (PotionEffect pe : p.getActivePotionEffects())
            p.removePotionEffect(pe.getType());
        for (PotionEffect pe : PotionEffects)
            p.addPotionEffect(pe);
    }

    private boolean isArmorValid(ItemStack[] armor) {
        return armor != null && armor.length == 4;
    }
}
