package net.beastcube.minigameapi.kits;

import net.beastcube.api.bukkit.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import javax.annotation.Nullable;

/**
 * @author BeastCube
 */
public class ItemStackKit extends Kit {

    private Material Type;
    private short Data;

    public ItemStackKit(String name, String description, @Nullable org.bukkit.inventory.Inventory inventory, @Nullable ItemStack[] armour, @Nullable PotionEffect[] potioneffects, int id, Material type, short data) {
        super(name, description, inventory, armour, potioneffects, id);
        Type = type;
        Data = data;
    }

    public Material getType() {
        return Type;
    }

    public short getData() {
        return Data;
    }

    public boolean isKit(ItemStack stack) {
        if (stack.getType() == getType() && stack.getDurability() == getData()) {
            if (stack.hasItemMeta()) {
                if (stack.getItemMeta().getDisplayName().equals(getName()))
                    return true;
            }
        }
        return false;

    }

    public ItemStack createItemStack() {
        return new ItemBuilder(getType()).amount(1).durability(getData()).name(getName()).lore(getKitDescription()).addAllFlags().build();
    }
}
