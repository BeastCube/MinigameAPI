package net.beastcube.minigameapi.kits;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.minigameapi.MinigameAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author BeastCube
 */
public class KitChooserInteractItem extends InteractItem implements KitChooser {

    private static KitChooserInteractItem kitChooser;

    public static KitChooserInteractItem getKitChooser() {
        if (kitChooser == null)
            kitChooser = new KitChooserInteractItem();
        return kitChooser;
    }

    private KitChooserInteractItem() {
        kitChooser = this;
        InteractItemManager.registerInteractItem(this);
    }

    @Override
    public Material getMaterial() {
        return Material.PAPER;
    }

    @Override
    public String getDisplayName() {
        return ChatColor.GOLD + "Kit-Wähler";
    }

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        ItemMenu menu = new ItemMenu(ChatColor.GOLD + "Kits", ItemMenu.Size.fit(KitChoosing.getKitsLength()), MinigameAPI.getInstance().getMinigame());
        for (int i = 0; i < KitChoosing.getKitsLength(); i++) {
            menu.setItem(i, new KitMenuItem(KitChoosing.getKit(i), p));
        }
        menu.open(p);
    }

    @Override
    public boolean isItem(ItemStack stack) {
        if (stack != null) {
            if (stack.hasItemMeta()) {
                return (getDisplayName().equals(stack.getItemMeta().getDisplayName()));
            }
        }
        return false;
    }

    @Override
    public void giveToPlayer(Player p) {
        p.getInventory().addItem(this.createItemStack());
    }

    @Override
    public void takeFromPlayer(Player p) {
        p.getInventory().removeItem(this.createItemStack());
    }
}
